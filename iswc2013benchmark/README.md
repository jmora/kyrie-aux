## bqr4obda: benchmarking query rewriting for ontology based data access

This is a collection of quick and dirty scripts to execute some query rewriting systems, collect the results and display them.
I'm sorry that they are quick and dirty but if they weren't quick and dirty then most probably they would not be whatsoever.
There is no available documentation, the best thing to do if you plan to modify or use them or face some problems is contacting me.
The documentation will thus be written as a FAQ on a reactive (aka [lazy](http://en.wikipedia.org/wiki/Lazy_evaluation)) way.
Please ask, you can contact me by e-mail at live.com prepending my username in this site, or simply open an issue.
