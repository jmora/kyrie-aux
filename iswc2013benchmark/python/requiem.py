from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal


class Requiem(OutputParser):
  def __init__(self, db, mode):
    self.db = db
    self.mode = mode
    self.db.execute('''create table if not exists requiem%s (
                              ontology text, query numeric,
                              timeData numeric, sizeData numeric,
                              timeUnfold numeric, sizeUnfold numeric,
                              timeUCQ numeric, sizeUCQ numeric)'''%mode)

  def test(self, ontology, qs, times):
    for q in qs:
      self.queryWrite(queries[ontology][q])
      for _ in range(times):
        self.__iteration(ontology,q+1)
      time.sleep(5)

  def __iteration(self, ontology, number):
    os.system("java -jar ../systems/requiem.jar query.txt %sontologies/%s.owl . %s > output.txt"%(base, ontology,self.mode))
    values = {}
    with open("output.txt", 'r') as f:
      for l in f:
        values.update(self.extract(l))
    self.db.execute('insert into requiem%s values (?,?,?,?,?,?,?,?)'%self.mode, (ontology, number,
                     values['timeSaturation'] - values['timeOntology'], values['sizeSaturation'],
                     values['timeUnfolding'] - values['timeOntology'], values['sizeUnfolding'],
                     values['timePruning'] - values['timeOntology'], values['sizePruning']))
  def extract(self, l):
    r = {}
    m = re.match(r"(?P<stage>\w+).*ed[^\d]+(?P<size>\d+) clauses.*time: (?P<time>\d+)", l)
    if m is None:
      return r
    m = m.groupdict()
    r['time'+m['stage']] = int(m['time'])
    r['size'+m['stage']] = int(m['size'])
    return r
    
    