#!/usr/bin/python3

import sqlite3, data, re
from functools import reduce

queries = data.queries

curryin = lambda x: lambda y: y in x
compose = lambda *fs: lambda x: reduce(lambda p, f: f(p), fs, x)

class PartialPath:
  def __init__(self, query, path = [], activeVar = None):
    self.query = query
    self.path = path
    self.distinguised = query[0].vars
    self.activeVar = activeVar
    self._expansion = None
  
  @property
  def isFinal(self):
    return self.path and not self.expansion

  def _getNextVar(self, atom):
    if atom.nvars == 1:
      return None
    elif atom.nvars == 2:
      if self.activeVar is not None:
        candidate = atom.vars[0] if atom.vars[1] == self.activeVar else atom.vars[1]
        return None if candidate in self.distinguised else candidate
      return atom.vars[0] if atom.vars[1] in self.distinguised else atom.vars[1]
    print("This program only works with binary predicates, it's very dependent on ontologies.")

  @property
  def solution(self):
    return (self.path, self.type)
  
  @property
  def type(self):
    if any(x in self.distinguised for x in self.path[-1].vars):
      return 'closed'
    if any(v in a.vars for a in self.query for v in self.path[-1].vars):
      return 'joined'
    return 'hanging'
  
  @property  
  def expansion(self):
    if self._expansion is None:
      self._expansion = self._getExpansion()
    return self._expansion
  
  def _getNext(self, atom):
    query = self.query[:]
    query.remove(atom)
    return PartialPath(query, self.path + [atom], self._getNextVar(atom))

  def _getExpansion(self):
    if self.activeVar is not None:
      return [self._getNext(a) for a in self.query[1:] if self.activeVar in a.vars]
    elif not self.path:
      return [self._getNext(a) for a in self.query if a.nvars == 2 and ((a.vars[0] in self.distinguised) != (a.vars[1] in self.distinguised))]
    return []

def backtracking(partialSolution):
  if partialSolution.isFinal:
    return [partialSolution.solution]
  return (s for e in partialSolution.expansion for s in backtracking(e))

class Atom:
  def __init__(self, atom):
    self.pred = re.findall('([\w\d-]+)\(.*\)', atom)[0]
    self.vars = re.findall('\?([\w\d-]+)', atom)
    self.nvars = len(self.vars)
  
  def __str__(self):
    return self.pred + str(self.vars)

  def __lt__(self, other):
    return str(self) < str(other)

  def __eq__(self, other):
    return str(self) == str(other)

  def __repr__(self):
    return str(self)

  def __hash__(self):
    return hash(str(self))

class QueryAnalyser:
  
  def __init__(self, query):
    self._sub = []
    atoms = [Atom(x) for x in re.findall('[\w\d-]+\([^\)]*\)', query)]
    self.atoms = atoms
    self.length = len(atoms) - 1
    self.distinguisedvars = set(atoms[0].vars)
    self.distinguised = len(self.distinguisedvars)
    varlist = [var for atom in atoms for var in atom.vars]
    self.vars = set(varlist)
    reusedvars = [varlist.remove(x) for x in self.vars]
    self.hangingvars = self.vars - set(varlist)
    self.hanging = len(self.hangingvars)
    self.existentialvars = (self.vars - self.distinguisedvars) - self.hangingvars
    self.existential = len(self.existentialvars)
    self.manypaths = list(backtracking(PartialPath(atoms)))
    self.propertypaths = self._removebadpaths(self.manypaths)
    self.paths = len(self.propertypaths)
    self.averageLength = 0 if not self.paths else sum(len(x[0]) - 1 for x in self.propertypaths) / self.paths
    self.open = len([p for p in self.propertypaths if p[1] == 'hanging'])
    self.closed = len([p for p in self.propertypaths if p[1] == 'closed'])
    self.joined = len([p for p in self.propertypaths if p[1] == 'joined'])
    if self.open + self.closed + self.joined != self.paths:
      print("Weird:\n\topen:\t%d\n\tclosed:\t%d\n\tjoined:\t%d\n\ttotal:\t%d\n\tfor:\t%s\n%s"%(self.open, self.closed, self.joined, self.paths, query, self.propertypaths))

  def _removebadpaths(self, paths): #ToDo: make this legible
    return [paths[i] for i in range(len(paths)) if ((len(paths[i][0]) > 2 or (len(paths[i][0]) > 1 and paths[i][0][-1].nvars > 1)) and all(str(sorted(p[0])) != str(sorted(paths[i][0])) for p in paths[i+1:]))]
  
  @property
  def subqueries(self):
    if (not self._sub and self.manypaths is not None):
      self._sub = self.getSubqueries()
    return len(self._sub)
  
  def getSubqueries(self):
    subqueries = []
    for p in self.manypaths:
      wasthere = False
      for i in range(len(subqueries)):
        if any(a in subqueries[i] for a in p[0]):
          subqueries[i] = subqueries[i] | set(p[0])
          wasthere = True
          break
      if (not wasthere):
        subqueries.append(set(p[0]))
    for a in self.atoms[1:]:
      if not any(a in s for s in subqueries):
        subqueries.append(set([a]))
    return subqueries
  
def populateTables():
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute('drop table query')
  c.execute('''create table if not exists query (ontology text, number integer, txt text, length integer,
    distinguisedVars integer, hangingVars integer, existentialVars integer, paths integer, openPaths integer,
    closedPaths integer, joinedPaths integer, averageLengthPath integer, subqueries integer)'''
  )
  for k in queries:
    for i in range(len(queries[k])):
      qa = QueryAnalyser(queries[k][i])
      c.execute('insert into query values (?,?,?,?,?,?,?,?,?,?,?,?,?)', (k, i+1, queries[k][i], qa.length,
        qa.distinguised, qa.hanging, qa.existential, qa.paths, qa.open, qa.closed, qa.joined, qa.averageLength, qa.subqueries)
      )
  conn.commit()
  c.close()


if __name__ == "__main__":
  populateTables()
