from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal

class Rapid(OutputParser):
  def __init__(self, db):
    self.db = db
    self.db.execute('''create table if not exists rapid (
                              ontology text, query numeric,
                              timeData numeric, sizeData numeric,
                              timeUCQ numeric, sizeUCQ numeric, timefull numeric, sizefull numeric)''')

  def extract(self, l):
    for e in ([r"Total time[^\d]+(?P<time>\d+)",
               r"Redundacy removal time[^\d]+(?P<ftime>\d+)",
               r"Core rewriting size[^\d]+(?P<size>\d+)",
               r"Rewriting size[^\d]+(?P<fsize>\d+)"]):
      m = re.match(e, l)
      if not m is None:
        return m.groupdict()
    return {}
  
  def test(self, ontology, qs, times):
    for q in qs:
      self.queryWrite(queries[ontology][q])
      for _ in range(times):
        self.__iteration(ontology,q+1)
      time.sleep(5)
  
  def __iteration(self, ontology, number):
    values = {}
    os.system("java -jar ../systems/rapid.jar F %sontologies/%s.owl query.txt output.txt"%(base, ontology))
    with open("output.txt", 'r') as f:
      for l in f:
        values.update(self.extract(l))
    values['timeUnfold'] = values['time']
    values['sizeUnfold'] = values['size']
    values['sizefull'] = values['fsize']
    values['timefull'] = values['ftime']
    os.system("java -jar ../systems/rapid.jar D %sontologies/%s.owl query.txt output.txt"%(base, ontology))
    with open("output.txt", 'r') as f:
      for l in f:
        values.update(self.extract(l))
    self.db.execute('insert into rapid values (?,?,?,?,?,?,?,?)', (ontology, number,
                     int(values['time']), int(values['size']),
                     int(values['timeUnfold']), int(values['sizeUnfold']), int(values['timeUnfold']) - int(values['timefull']), int(values['sizefull'])))
