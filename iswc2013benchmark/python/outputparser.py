import os, sqlite3, re, datetime, time, subprocess, threading, signal

class OutputParser():

  def getMilis(self, first, second):
    d = second - first
    return d.seconds * 1000 + d.microseconds / 1000

  def queryWrite(self, q):
    with open('query.txt', 'w') as f:
      f.write(q)
