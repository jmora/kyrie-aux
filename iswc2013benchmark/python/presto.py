from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal

class Presto(OutputParser):
  def __init__(self, db, times):
    self.db = db
    self.var = ['x','y','z','u','v','t', 'w', 's', 'r']
    self.db.execute('''create table if not exists presto%d (
                              ontology text, query integer,
                              timeData integer, sizeData integer,
                              timeUCQ integer, sizeUCQ integer,
                              timeOptUCQ integer, sizeOptUCQ integer)'''%times)
    self.variants = [{'base':"%s\nDP\n", 'lines':0}, {'base':"%s\nUCQ\nN\n", 'lines':0}, {'base':"%s\nUCQ\nY\n", 'lines':0}]
    self.times = times
    self.bases = ({ "V": "http://vicodi.org/ontology#",
                    "S": "http://www.owl-ontologies.com/Ontology1207768242.owl#",
                    "U": "http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#",
                    "A": "file:/home/aurona/0AlleWerk/Navorsing/Ontologies/NAP/NAP#",
                    "P1": "http://www.semanticweb.org/ontologies/2008/8/24/Ontology1222256119496.owl#",
                    "P5": "http://www.semanticweb.org/ontologies/2008/8/24/Ontology1222256119496.owl#",
                    "UX": "http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#",
                    "UXE": "http://www.lehigh.edu/~zhp2/2004/0401/univ-bench.owl#",
                    "P5X": "http://www.semanticweb.org/ontologies/2008/8/24/Ontology1222256119496.owl#",
                    "P5XE": "http://www.semanticweb.org/ontologies/2008/8/24/Ontology1222256119496.owl#",
                    "AX": "file:/home/aurona/0AlleWerk/Navorsing/Ontologies/NAP/NAP#",
                    "AXE": "file:/home/aurona/0AlleWerk/Navorsing/Ontologies/NAP/NAP#",
                    "AXEb": "file:/home/aurona/0AlleWerk/Navorsing/Ontologies/NAP/NAP#",
                    "AXEc": "file:/home/aurona/0AlleWerk/Navorsing/Ontologies/NAP/NAP#"})
    
  
  def test(self, ontology, qs, times):
    for q in qs:
      nq = self.sparqlfy(queries[ontology][q], self.bases[ontology])
      for _ in range(times):
        self.__iteration(ontology, nq, q+1)
  
  def __iteration(self, ontology, query, number):
    self.r = []
    print("Executing query %d on ontology %s."%(number, ontology))
    thread = threading.Thread(target=self.testSub, args=(ontology, query, number))
    thread.start()
    thread.join(55000)
    if thread.is_alive():
      self.proc.terminate()
      thread.join()    
      print('Presto was killed, ontology %s, query %d'%(ontology, number))
      return
    r = self.r
    try:
      if (r[5] > 1):
        self.db.execute('insert into presto%d values (?,?,?,?,?,?,?,?)'%self.times, (ontology, number, r[0], r[1], r[2], r[3], r[4], r[5]))
    except:
      print("Insertion didn't work quite well...")
  
  def testSub(self, ontology, query, number):  
    r = []
    for v in self.variants:
      self.proc = subprocess.Popen(["java", "-jar", "../systems/presto/QueryReformulator20121121.jar", "../ontologies/%s.owl"%ontology],
                            stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
      inbytes = bytes(v['base']%query, 'utf-8')
      instring = ""
      for _ in range(self.times):
        instring += v['base']%query
      inbytes = bytes(instring + 'quit\n', 'utf-8')
      (out, err)= self.proc.communicate(inbytes)
      soutput = out.decode('utf-8')
      lines = len(re.findall(':-', soutput))//self.times
      lasttime = re.findall('\[OUTPUT\][^\d]*(\d+)\r', soutput)[-1]
      r += [lasttime, lines]
    self.r = r
    
  def processAtom(self, atom, base):
    (h, b) = (re.findall('([\w\-\d]+)\(([^\)]+)\)', atom))[0]
    b = b.split(',')
    sc = '<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>'
    if (len(b) == 1):
      return '%s %s <%s%s> . '%(b[0], sc, base, h)
    return '%s <%s%s> %s . '%(b[0],base,h, b[1])

  def processHead(self, head):
    return re.sub(',', ' ', str((re.findall('[\w\-\d]+\(([^\)]+)\)', head))[0]))
    
  def sparqlfy(self, conjunctive, base):
    (h, b) = (re.findall('(.*?) (?:\<\-) (.*)', conjunctive))[0]
    b = b.split(', ')
    r = 'select %s where {'%self.processHead(h)
    for e in b:
      r += self.processAtom(e, base)
    r = re.sub('\?(\d)', lambda x: '?'+self.var[int(x.group(1))] , r)
    return r + '}'
