from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal

class Kyrie(OutputParser):
  def __init__(self, db, v, maps = False):
    self.db = db
    self.v = v
    self.maps = maps
    self.db.execute('''create table if not exists kyriev%d (
                              ontology text, query numeric,
                              timeData numeric, sizeData numeric,
                              timeUCQ numeric, sizeUCQ numeric)'''%v)
    self.db.execute('''create table if not exists kyrieprev%d (
                              ontology text, timePre numeric,
                              sizePre numeric, sizeOrig numeric )'''%v)


  def test(self, ontology, qs, times):
    for q in qs:
      self.queryWrite(queries[ontology][q])
      for _ in range(times):
        try:
          self.__iteration(ontology,q+1)
        except Exception as e:
          print("Error:" + str(e))
      time.sleep(5)
  
  def __iteration(self, ontology, number):
    if (self.v <= 18):
      if (not self.maps):
        os.system("java -jar ../systems/kyriev%d.jar query.txt ../ontologies/%s.owl . F M > output.txt"%(self.v, ontology))
      else:
        os.system("java -jar ../systems/kyriev%d.jar query.txt ../ontologies/%s.owl . F P %smape.txt > output.txt"%(self.v, ontology, ontology))
    else:
      if (not self.maps):
        os.system("java -jar ../systems/kyriev%d.jar query.txt ../ontologies/%s.owl > output.txt"%(self.v, ontology))
      else:
        os.system("java -jar ../systems/kyriev%d.jar query.txt ../ontologies/%s.owl %smape.txt > output.txt"%(self.v, ontology, ontology))
    values = {}
    with open("output.txt", 'r') as f:
      for l in f:
        values.update(self.extract(l))
    if (number == 1):
      self.db.execute('insert into kyrieprev%d values (?,?,?,?)'%self.v, (ontology,
                       self.getMilis(values['timeLoading'], values['timePreprocessing']),
                       values['sizePreprocessing'], values['sizeLoading']))
    self.db.execute('insert into kyriev%d values (?,?,?,?,?,?)'%self.v, (ontology, number,
                     self.getMilis(values['timeReloading'], values['timeSaturation']), values['sizeSaturation'],
                     self.getMilis(values['timeReloading'], values['timeUnfolding']), values['sizeUnfolding']))

  def extract(self, l):
    r = {} # sorry for next line, but that's the way I like it.
    if (self.v <= 18):
      m = re.match(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+):(?P<second>\d+),(?P<microsecond>\d+).*PreprocessRewriter - (?P<stage>\w+) completed \((?P<size>\d+) clauses\)", l)
    else:
      m = re.match(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+):(?P<second>\d+),(?P<microsecond>\d+).*Rewriter - (?P<stage>\w+) completed \((?P<size>\d+) clauses\)", l)
    if m is None:
      return r
    m = m.groupdict()
    params = ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']
    d = {}
    for p in params:
      d[p] = int(m[p])
    d['microsecond'] = d['microsecond'] * 1000 # Yes, I lied.
    r['time'+m['stage']] = datetime.datetime(**d)
    r['size'+m['stage']] = int(m['size'])
    return r