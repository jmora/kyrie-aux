from random import choice
from functools import reduce
import sqlite3

k = 'key'
v = 'value'
c = 'cluster'
smaximins = '''select max(length)*1.0 as maxlength, min(length)*1.0 as minlength,
  max(distinguisedVars)*1.0 as maxdistinguisedVars, min(distinguisedVars)*1.0 as mindistinguisedVars,
  max(hangingVars)*1.0 as maxhangingVars, min(hangingVars)*1.0 as minhangingVars,
  max(existentialVars)*1.0 as maxexistentialVars, min(existentialVars)*1.0 as minexistentialVars,
  max(paths)*1.0 as maxpaths, min(paths)*1.0 as minpaths,
  max(openPaths)*1.0 as maxopenPaths, min(openPaths)*1.0 as minopenPaths,
  max(closedPaths)*1.0 as maxclosedPaths, min(closedPaths)*1.0 as minclosedPaths,
  max(joinedPaths)*1.0 as maxjoinedPaths, min(joinedPaths)*1.0 as minjoinedPaths,
  max(averageLengthPath)*1.0 as maxaverageLengthPath, min(averageLengthPath)*1.0 as minaverageLengthPath,
  max(subqueries)*1.0 as maxsubqueries, min(subqueries)*1.0 as minsubqueries from query'''

def maximino():
  res = []
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute(smaximins)
  for row in c:
    res = list(row)
  conn.commit()
  c.close()
  return res

diff = lambda x: lambda y: sum((x[i] - y[i])**2 for i in range(len(x)))/len(x)
vdiff = lambda x: lambda y: [(x[i] - y[i])**2 for i in range(len(x))]
plus = lambda x: lambda y: [x[i] + y[i] for i in range(len(x))]
times = lambda x: lambda y: [x * e for e in y]
avg = lambda x: sum(x) / len(x)
minus = lambda x: lambda y: plus(times(-1)(x))(y)

denormalizerer = lambda x: lambda y: [(y[i]*(x[i*2] - x[i*2+1])) + x[i*2+1] for i in range(len(y))]
denormalizer = denormalizerer(maximino())

# data is a list of dictionaries with key (the primary key of each element) and value (a list of normalised integers)
def kmeans(data, n):
  means = [choice(data)[v] for i in range(n)]
  oldmeans = []
  while means != oldmeans:
    oldmeans = means
    nmeans = dict(enumerate(means))
    data = [{k:e[k], v:e[v], c:min(nmeans, key = lambda x: diff(e[v])(nmeans[x]))} for e in data]
    sums = [([0.0 for i in range(len(data[0][v]))], 0) for i in range(n)]
    for e in data:
      sums[e[c]] = (plus(sums[e[c]][0])(e[v]), sums[e[c]][1] + 1)
    means = [[ e / s[1] if s[1] else 0.5 for e in s[0]] for s in sums]
  return (data, means)
    
def getNormalisedData():
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute('''select q.ontology, q.number,
  (q.length - q2.minlength) / (q2.maxlength - q2.minlength),
  (q.distinguisedVars - q2.mindistinguisedVars) / (q2.maxdistinguisedVars - q2.mindistinguisedVars),
  (q.hangingVars - q2.minhangingVars) / (q2.maxhangingVars - q2.minhangingVars),
  (q.existentialVars - q2.minexistentialVars) / (q2.maxexistentialVars - q2.minexistentialVars),
  (q.paths - q2.minpaths) / (q2.maxpaths - q2.minpaths),
  (q.openPaths - q2.minopenPaths) / (q2.maxopenPaths - q2.minopenPaths),
  (q.closedPaths - q2.minclosedPaths) / (q2.maxclosedPaths - q2.minclosedPaths),
  (q.joinedPaths - q2.minjoinedPaths) / (q2.maxjoinedPaths - q2.minjoinedPaths),
  (q.averageLengthPath - q2.minaverageLengthPath) / (q2.maxaverageLengthPath - q2.minaverageLengthPath),
  (q.subqueries - q2.minsubqueries) / (q2.maxsubqueries - q2.minsubqueries) from query as q join (%s) as q2
  on 1 == 1'''%smaximins)
  data = []
  for row in c:
    data.append({k:row[:2], v:row[2:]})
  conn.commit()
  c.close()
  return data

# ToDo: this doesn't look right
def explainedVariance(data, means):
  # http://en.wikipedia.org/wiki/F-test
  sums = [([0.0 for i in range(len(data[0][v]))], 0) for i in range(len(means)+1)]
  for e in data:
    sums[e[c]] = (plus(sums[e[c]][0])(e[v]), sums[e[c]][1] + 1)
    sums[-1] = (plus(sums[-1][0])(e[v]), sums[-1][1] + 1)
  avgs = [[(ss/s[1]) if s[1] else 0 for ss in s[0]] for s in sums]
  sdiff = [[0.0 for i in range(len(data[0][v]))] for i in range(len(means)+1)]
  for e in data:
    sdiff[e[c]] = plus(sdiff[e[c]])(vdiff(e[v])(avgs[e[c]]))
    sdiff[-1] = plus(sdiff[-1])(vdiff(e[v])(avgs[-1]))
  variance = [[(e/sums[i][1]) if sums[i][1] else 0 for e in sdiff[i]] for i in range(len(sdiff))]
  #explainedVariances = times(sums[i][1]/(len(means)-1))(reduce(lambda x,y: plus(x)(y), [vdiff(avgs[i])(avgs[-1]) for i in range(len(means))]))
  explainedVariances = times(1/(len(means)-1))(reduce(lambda x,y: plus(x)(y), [times(sums[i][1])(vdiff(avgs[i])(avgs[-1])) for i in range(len(means))]))
  #unexplainedVariances = reduce(lambda x,y: plus(x)(y),[times(1/(sums[-1][1] - len(means)))(sdiff[i]) for i in range(len(means))])
  unexplainedVariances = times(1/(sums[-1][1] - len(means)))(reduce(lambda x,y: plus(x)(y),sdiff[:-1]))
  res = (avg([explainedVariances[i] / (unexplainedVariances[i] + 0.0001) for i in range(len(means))]))
  return (len(means), res, explainedVariances, unexplainedVariances)
  

 
normaldata = getNormalisedData()
for i in range(2,10):
  (_, ev, _, _) = explainedVariance(*kmeans(normaldata, i))
  print("explained variance for %d clusters: %d"%(i, int(ev)))

print("\nclustering results for 6 clusters:")
i = 0
prettify = lambda n: ("%.4f" % n).rstrip("0").rstrip(".")
for e in kmeans(getNormalisedData(), 6)[1]:
  r = denormalizer(e)
  i += 1
  r = tuple([i] + [prettify(n) for n in r])
  print('''cluster %d:\n\tquery length: %s\n\tdistinguished variables: %s\n\thanging variables: %s\n\texistential variables: %s
\tpaths: %s\n\topen paths: %s\n\tclosed paths: %s\n\tjoined paths: %s\n\taverage path length: %s\n\tsubqueries: %s'''%r)
  
