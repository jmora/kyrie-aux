#!/usr/bin/python3

import sqlite3

pretty = lambda x: str(int(x)) if x.__class__.__name__ == 'float' else "N/A" if x is None else str(x)
notsopretty = lambda x: str(float(x)) if x.__class__.__name__ == 'float' else "N/A" if x is None else str(x)

def maintable():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table*}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{ccc|c|c|c|c|c|c|c|c|}\\cline{3-11}
\\multicolumn{2}{c|}{}&\\multicolumn{3}{c|}{REQUIEM (F mode)}&\\multicolumn{3}{ c|}{Rapid (D\\&F modes)}&\\multicolumn{3}{c|}{kyrie} \\\\ \\cline{3-11}
\\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{Datalog}&UCQ&\\multicolumn{2}{c|}{Datalog}&UCQ&\\multicolumn{2}{c|}{Datalog}&UCQ \\\\ \\hline
\\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&time&size&time&time&size&time&time&size&time \\\\ \\hline'''
  footer ='''\n\\end{tabular}
\\smallskip
\\caption{Results of the evaluation}
\\label{tab:bigresults}
\\vspace{-5pt}
\\end{table*}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query, avg(rqr.timeData), avg(rqr.sizeData), avg(rqr.timeUCQ),
                                 avg(rap.timeData), avg(rap.sizeData), avg(rap.timeUCQ),
                                 avg(kyr.timeData), avg(kyr.sizeData), avg(kyr.timeUCQ)
               from requiem as rqr join rapid as rap on rqr.ontology = rap.ontology and rqr.query = rap.query
                                   join kyriev11 as kyr on kyr.ontology = rqr.ontology and kyr.query = rqr.query
               where replace(kyr.ontology, "E", "") = kyr.ontology
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '3' else row[0], ) + row[1:] + ('\\hline' if row[1] == '5' else ' ',))
  res += footer
  return res

def newcases():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{ccc|c|c|c|c|c|c|c|c|c|c|c|}\\cline{3-14}
\\multicolumn{2}{c|}{}&\\multicolumn{4}{c|}{REQUIEM (F mode)}&\\multicolumn{4}{ c|}{Rapid (D\\&F modes)}&\\multicolumn{4}{c|}{kyrie} \\\\ \\cline{3-14}
\\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ} \\\\ \\hline
\\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&time&size&time&size&time&size&time&size&time&size&time&size \\\\ \\hline'''
  footer ='''\n\\end{tabular}
\\smallskip
\\caption{Results obtained with the extended ontologies}
\\label{tab:newresults}
\\vspace{-5pt}
\\end{table}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query, rqr.timeData, rqr.sizeData, rqr.timeUCQ, rqr.sizeUCQ,
                                 rap.timeData, rap.sizeData, rap.timeUCQ, rap.sizeUCQ,
                                 kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ
               from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from rapid group by ontology, query) as rap
               join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from kyriev11 group by ontology, query) as kyr on kyr.ontology = rap.ontology and kyr.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from requiem group by ontology, query) as rqr on rqr.ontology = rap.ontology and rqr.query = rap.query
               where replace(kyr.ontology, "E", "") != kyr.ontology
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '3' else row[0], ) + row[1:] + ('\\hline' if row[1] == '5' else ' ',))
  res += footer
  return res
  
def prepro():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{c|c|c|c}\\cline{2-3}
&\\multicolumn{2}{c|}{Number of clauses}& \\\\ \\hline
\\multicolumn{1}{|c|}{$\\mathcal{O}$}&before&after&\\multicolumn{1}{c|}{time} \\\\ \\hline'''
  footer = '''\\end{tabular}
\\smallskip
\\caption{Results of ontology preprocessing in kyrie, number of clauses and miliseconds.}
\\label{tab:prepro}
\\vspace{-5pt}
\\end{table}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&%s&%s&\\multicolumn{1}{c|}{%s} \\\\ \\hline'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute('''select kyr.ontology, avg(kyr.sizeOrig), avg(kyr.sizePre), avg(kyr.timePre)
               from kyrieprev10 as kyr group by kyr.ontology''')
  res = header
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%row
  res += footer
  return res

def others():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
%\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{|c|c|c|c|c|c|c|} \\hline
$\\mathcal{O}$&$q$&Presto&kyrie$_{\\mbox{\\tiny datalog}}$&kyrie$_{\\mbox{\\tiny  UCQ}}$&Nyaya&Nyaya$_{\\mbox{\\tiny  opt}}$\\\\ \\hline'''
  rowtemplate = '\n%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  footer='''\\end{tabular}
\\smallskip
\\caption{Comparison of results provided in other papers, number of clauses}
\\label{tab:paperresults}
\\vspace{-5pt}
\\end{table}}'''
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute('''select kyr.ontology, kyr.query, avg(pre.sizeData), avg(kyr.sizeData), avg(kyr.sizeUCQ), avg(nya.sizeBasic), avg(nya.sizeOpt)
               from kyriev11 as kyr left outer join presto as pre on kyr.ontology = pre.ontology and kyr.query = pre.query
                                   left outer join nyaya as nya on kyr.ontology = nya.ontology and kyr.query = nya.query
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''') 
  res = header
  for row in [tuple(map(pretty, e)) for e in c if "E" not in e[0]]:
    res += rowtemplate%((row[0] if row[1] == '3' else ' ',)+row[1:]+('\\hline' if row[1] == '5' else ' ', ))
  res += footer
  return res
  
def html():
  mainTemplate = '''<html>
  <head>
    <title>Results for the evaluation of kyrie.</title>
    <link rel="stylesheet" type="text/css" href="files/style.css" />
    <script src="files/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="files/highlighter.js" type="text/javascript"></script>
  </head>
  <body>
    <a href="http://www.oeg-upm.net/"><img style="float:left" src="files/Logo_OEG.png" /></a>
    <h2>Evaluation results for query rewriting</h2>
    <p>This page contains the results obtained from using several systems to rewrite several queries according to some ontologies. Choose the ontologies and systems to compare and check the results. If you have questions or suggestions please write me at <img id="mail" src="files/youshoulddothistoo.png" /></p>
    <p><a href="./queryanalysis.html">The characteristics of the queries used in this bechmark are presented separately (click here).</a></p>
    <div id="menu"><div>Choose your options.</div><br/ >
      <div><b>Systems:</b></div>
      <div><input type="checkbox" name="system" value="requiemn" />REQUIEM (N)</div>
      <div><input type="checkbox" name="system" value="requiemf" />REQUIEM (F)</div>
      <div><input type="checkbox" name="system" value="requiemg" />REQUIEM (G)</div>
      <div><input type="checkbox" name="system" value="rapid" />Rapid</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev2" />kyriev2</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev9" />kyriev9</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev10" />kyriev10</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev11" />kyriev11</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev12" />kyriev12</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev14" />kyriev14</div>
      <div><input type="checkbox" name="system" value="kyrie" />kyrie</div>
      <div class="hidden"><input type="checkbox" name="system" value="kyriev16" />kyriev16</div>
      <div><input type="checkbox" name="system" value="presto1" />Presto</div>
      <div><input type="checkbox" name="system" value="presto2" />Presto2</div>
      <div><input type="checkbox" name="system" value="nyaya" />Nyaya</div>
      <div><input type="checkbox" name="system" value="clipper" />Clipper</div>
      <br />
      <div><b>Ontologies:</b></div>%s
    </div>
    <div id="tablecontainer">
      <table>
        <colgroup>
          <col />
          <col />
          <col />
          <col system="requiemn" class="startsSystem hidden" />
          <col system="requiemn" class="hidden" />
          <col system="requiemn" class="hidden" />
          <col system="requiemn" class="hidden" />
          <col system="requiemn" class="hidden" />
          <col system="requiemn" class="hidden" />
          <col system="requiemf" class="startsSystem hidden" />
          <col system="requiemf" class="hidden" />
          <col system="requiemf" class="hidden" />
          <col system="requiemf" class="hidden" />
          <col system="requiemf" class="hidden" />
          <col system="requiemf" class="hidden" />
          <col system="requiemg" class="startsSystem hidden" />
          <col system="requiemg" class="hidden" />
          <col system="requiemg" class="hidden" />
          <col system="requiemg" class="hidden" />
          <col system="requiemg" class="hidden" />
          <col system="requiemg" class="hidden" />
          <col system="rapid" class="startsSystem hidden" />
          <col system="rapid" class="hidden" />
          <col system="rapid" class="hidden" />
          <col system="rapid" class="hidden" />
          <col system="kyriev2" class="startsSystem hidden" />
          <col system="kyriev2" class="hidden" />
          <col system="kyriev2" class="hidden" />
          <col system="kyriev2" class="hidden" />
          <col system="kyriev2" class="hidden" />
          <col system="kyriev2" class="hidden" />
          <col system="kyriev9" class="startsSystem hidden" />
          <col system="kyriev9" class="hidden" />
          <col system="kyriev9" class="hidden" />
          <col system="kyriev9" class="hidden" />
          <col system="kyriev9" class="hidden" />
          <col system="kyriev9" class="hidden" />
          <col system="kyriev10" class="startsSystem hidden" />
          <col system="kyriev10" class="hidden" />
          <col system="kyriev10" class="hidden" />
          <col system="kyriev10" class="hidden" />
          <col system="kyriev10" class="hidden" />
          <col system="kyriev10" class="hidden" />
          <col system="kyriev11" class="startsSystem hidden" />
          <col system="kyriev11" class="hidden" />
          <col system="kyriev11" class="hidden" />
          <col system="kyriev11" class="hidden" />
          <col system="kyriev11" class="hidden" />
          <col system="kyriev11" class="hidden" />
          <col system="kyriev12" class="startsSystem hidden" />
          <col system="kyriev12" class="hidden" />
          <col system="kyriev12" class="hidden" />
          <col system="kyriev12" class="hidden" />
          <col system="kyriev12" class="hidden" />
          <col system="kyriev12" class="hidden" />
          <col system="kyriev14" class="startsSystem hidden" />
          <col system="kyriev14" class="hidden" />
          <col system="kyriev14" class="hidden" />
          <col system="kyriev14" class="hidden" />
          <col system="kyriev14" class="hidden" />
          <col system="kyriev14" class="hidden" />
          <col system="kyrie" class="startsSystem hidden" />
          <col system="kyrie" class="hidden" />
          <col system="kyrie" class="hidden" />
          <col system="kyrie" class="hidden" />
          <col system="kyrie" class="hidden" />
          <col system="kyrie" class="hidden" />
          <col system="kyriev16" class="startsSystem hidden" />
          <col system="kyriev16" class="hidden" />
          <col system="kyriev16" class="hidden" />
          <col system="kyriev16" class="hidden" />
          <col system="kyriev16" class="hidden" />
          <col system="kyriev16" class="hidden" />
          <col system="presto1" class="startsSystem hidden" />
          <col system="presto1" class="hidden" />
          <col system="presto1" class="hidden" />
          <col system="presto1" class="hidden" />
          <col system="presto1" class="hidden" />
          <col system="presto1" class="hidden" />
          <col system="presto2" class="startsSystem hidden" />
          <col system="presto2" class="hidden" />
          <col system="presto2" class="hidden" />
          <col system="presto2" class="hidden" />
          <col system="presto2" class="hidden" />
          <col system="presto2" class="hidden" />
          <col system="nyaya" class="startsSystem hidden" />
          <col system="nyaya" class="hidden" />
          <col system="nyaya" class="hidden" />
          <col system="nyaya" class="hidden" />
          <col system="clipper" class="startsSystem hidden" />
          <col system="clipper" class="hidden" />
          <col class="startsSystem" />
          <col />
          <col />
        </colgroup>
        <thead>
          <tr>
            <th>O</th>
            <th>q</th>
            <th>Initial number of clauses</th>
            <th class="hidden">Requiem (N) datalog time (ms)</th>
            <th class="hidden">Requiem (N) datalog clauses</th>
            <th class="hidden">Requiem (N) unfold time (ms)</th>
            <th class="hidden">Requiem (N) unfold clauses</th>
            <th class="hidden">Requiem (N) UCQ time (ms)</th>
            <th class="hidden">Requiem (N) UCQ clauses</th>
            <th class="hidden">Requiem (F) datalog time (ms)</th>
            <th class="hidden">Requiem (F) datalog clauses</th>
            <th class="hidden">Requiem (F) unfold time (ms)</th>
            <th class="hidden">Requiem (F) unfold clauses</th>
            <th class="hidden">Requiem (F) UCQ time (ms)</th>
            <th class="hidden">Requiem (F) UCQ clauses</th>
            <th class="hidden">Requiem (G) datalog time (ms)</th>
            <th class="hidden">Requiem (G) datalog clauses</th>
            <th class="hidden">Requiem (G) unfold time (ms)</th>
            <th class="hidden">Requiem (G) unfold clauses</th>
            <th class="hidden">Requiem (G) UCQ time (ms)</th>
            <th class="hidden">Requiem (G) UCQ clauses</th>
            <th class="hidden">Rapid datalog time (ms)</th>
            <th class="hidden">Rapid datalog clauses</th>
            <th class="hidden">Rapid UCQ time (ms)</th>
            <th class="hidden">Rapid UCQ clauses</th>
            <th class="hidden">kyriev2 preprocess time (ms)</th>
            <th class="hidden">kyriev2 preprocess clauses</th>
            <th class="hidden">kyriev2 datalog time (ms)</th>
            <th class="hidden">kyriev2 datalog clauses</th>
            <th class="hidden">kyriev2 UCQ time (ms)</th>
            <th class="hidden">kyriev2 UCQ clauses</th>
            <th class="hidden">kyriev9 preprocess time (ms)</th>
            <th class="hidden">kyriev9 preprocess clauses</th>
            <th class="hidden">kyriev9 datalog time (ms)</th>
            <th class="hidden">kyriev9 datalog clauses</th>
            <th class="hidden">kyriev9 UCQ time (ms)</th>
            <th class="hidden">kyriev9 UCQ clauses</th>
            <th class="hidden">kyriev10 preprocess time (ms)</th>
            <th class="hidden">kyriev10 preprocess clauses</th>
            <th class="hidden">kyriev10 datalog time (ms)</th>
            <th class="hidden">kyriev10 datalog clauses</th>
            <th class="hidden">kyriev10 UCQ time (ms)</th>
            <th class="hidden">kyriev10 UCQ clauses</th>
            <th class="hidden">kyriev11 preprocess time (ms)</th>
            <th class="hidden">kyriev11 preprocess clauses</th>
            <th class="hidden">kyriev11 datalog time (ms)</th>
            <th class="hidden">kyriev11 datalog clauses</th>
            <th class="hidden">kyriev11 UCQ time (ms)</th>
            <th class="hidden">kyriev11 UCQ clauses</th>
            <th class="hidden">kyriev12 preprocess time (ms)</th>
            <th class="hidden">kyriev12 preprocess clauses</th>
            <th class="hidden">kyriev12 datalog time (ms)</th>
            <th class="hidden">kyriev12 datalog clauses</th>
            <th class="hidden">kyriev12 UCQ time (ms)</th>
            <th class="hidden">kyriev12 UCQ clauses</th>
            <th class="hidden">kyriev14 preprocess time (ms)</th>
            <th class="hidden">kyriev14 preprocess clauses</th>
            <th class="hidden">kyriev14 datalog time (ms)</th>
            <th class="hidden">kyriev14 datalog clauses</th>
            <th class="hidden">kyriev14 UCQ time (ms)</th>
            <th class="hidden">kyriev14 UCQ clauses</th>
            <th class="hidden">kyrie preprocess time (ms)</th>
            <th class="hidden">kyrie preprocess clauses</th>
            <th class="hidden">kyrie datalog time (ms)</th>
            <th class="hidden">kyrie datalog clauses</th>
            <th class="hidden">kyrie UCQ time (ms)</th>
            <th class="hidden">kyrie UCQ clauses</th>
            <th class="hidden">kyriev16 preprocess time (ms)</th>
            <th class="hidden">kyriev16 preprocess clauses</th>
            <th class="hidden">kyriev16 datalog time (ms)</th>
            <th class="hidden">kyriev16 datalog clauses</th>
            <th class="hidden">kyriev16 UCQ time (ms)</th>
            <th class="hidden">kyriev16 UCQ clauses</th>
            <th class="hidden">Presto datalog time (ms)</th>
            <th class="hidden">Presto datalog clauses</th>
            <th class="hidden">Presto UCQ time (ms)</th>
            <th class="hidden">Presto UCQ clauses</th>
            <th class="hidden">Presto opt UCQ time (ms)</th>
            <th class="hidden">Presto opt UCQ clauses</th>
            <th class="hidden">Presto (2nd run) datalog time (ms)</th>
            <th class="hidden">Presto (2nd run) datalog clauses</th>
            <th class="hidden">Presto (2nd run) UCQ time (ms)</th>
            <th class="hidden">Presto (2nd run) UCQ clauses</th>
            <th class="hidden">Presto (2nd run) opt UCQ time (ms)</th>
            <th class="hidden">Presto (2nd run) opt UCQ clauses</th>
            <th class="hidden">Nyaya time (ms)</th>
            <th class="hidden">Nyaya clauses</th>
            <th class="hidden">Nyaya joins</th>
            <th class="hidden">Nyaya atoms</th>
            <th class="hidden">Clipper time (ms)</th>
            <th class="hidden">Clipper clauses</th>
            <th>Initial number of clauses</th>
            <th>q</th>
            <th>O</th>
          <tr>
        </thead>
        <tbody>%s
        </tbody>
      </table>
    </div>
    <div id="container"><div id="querysnippet"> </div></div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
  </body>'''
  rowTemplate = ''
  totalcols = 94
  for i in range(totalcols):
    rowTemplate += '<td%s>%s</td>'%('' if i%(totalcols-3)<3 else ' class="hidden"', '%s')
  rowTemplate = '\n        <tr %s>%s</tr>'%('class="%s hidden" ontology="%s" query="%s"', rowTemplate)
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute('''select rap.ontology, query.txt, rap.ontology, query.number, kyp10.sizeOrig,
       rqrn.timeData, rqrn.sizeData, rqrn.timeUnfold, rqrn.sizeUnfold, rqrn.timeUCQ, rqrn.sizeUCQ,
       rqrf.timeData, rqrf.sizeData, rqrf.timeUnfold, rqrf.sizeUnfold, rqrf.timeUCQ, rqrf.sizeUCQ,
       rqrg.timeData, rqrg.sizeData, rqrg.timeUnfold, rqrg.sizeUnfold, rqrg.timeUCQ, rqrg.sizeUCQ,
       rap.timeData, rap.sizeData, rap.timeUCQ, rap.sizeUCQ,
       kyp2.timePre, kyp2.sizePre, ky2.timeData, ky2.sizeData, ky2.timeUCQ, ky2.sizeUCQ,
       kyp9.timePre, kyp9.sizePre, ky9.timeData, ky9.sizeData, ky9.timeUCQ, ky9.sizeUCQ,
       kyp10.timePre, kyp10.sizePre, ky10.timeData, ky10.sizeData, ky10.timeUCQ, ky10.sizeUCQ,
       kyp11.timePre, kyp11.sizePre, ky11.timeData, ky11.sizeData, ky11.timeUCQ, ky11.sizeUCQ,
       kyp12.timePre, kyp12.sizePre, ky12.timeData, ky12.sizeData, ky12.timeUCQ, ky12.sizeUCQ,
       kyp14.timePre, kyp14.sizePre, ky14.timeData, ky14.sizeData, ky14.timeUCQ, ky14.sizeUCQ,
       kyp15.timePre, kyp15.sizePre, ky15.timeData, ky15.sizeData, ky15.timeUCQ, ky15.sizeUCQ,
       kyp16.timePre, kyp16.sizePre, ky16.timeData, ky16.sizeData, ky16.timeUCQ, ky16.sizeUCQ,
       pre1.timeData, pre1.sizeData, pre1.timeUCQ, pre1.sizeUCQ, pre1.timeOptUCQ, pre1.sizeOptUCQ,
       pre2.timeData, pre2.sizeData, pre2.timeUCQ, pre2.sizeUCQ, pre2.timeOptUCQ, pre2.sizeOptUCQ,
       nya.time, nya.sizequeries, nya.sizejoins, nya.sizeatoms,
       cli.time, cli.size, kyp10.sizeOrig, rap.query, rap.ontology
from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from rapid group by ontology, query) as rap
  left outer join query on rap.ontology = query.ontology and rap.query = query.number
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev9 group by ontology) as kyp9 on kyp9.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev2 group by ontology) as kyp2 on kyp2.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre, avg(sizeOrig) as sizeOrig from kyrieprev10 group by ontology) as kyp10 on kyp10.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev11 group by ontology) as kyp11 on kyp11.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev12 group by ontology) as kyp12 on kyp12.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev14 group by ontology) as kyp14 on kyp14.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev15 group by ontology) as kyp15 on kyp15.ontology = rap.ontology
  left outer join (select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieprev16 group by ontology) as kyp16 on kyp16.ontology = rap.ontology
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUnfold) as timeUnfold, avg(sizeUnfold) as sizeUnfold, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from requiemn group by ontology, query) as rqrn on rqrn.ontology = rap.ontology and rqrn.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUnfold) as timeUnfold, avg(sizeUnfold) as sizeUnfold, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from requiemf group by ontology, query) as rqrf on rqrf.ontology = rap.ontology and rqrf.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUnfold) as timeUnfold, avg(sizeUnfold) as sizeUnfold, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from requiemg group by ontology, query) as rqrg on rqrg.ontology = rap.ontology and rqrg.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev2 group by ontology, query) as ky2 on ky2.ontology = rap.ontology and ky2.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev9 group by ontology, query) as ky9 on ky9.ontology = rap.ontology and ky9.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev10 group by ontology, query) as ky10 on ky10.ontology = rap.ontology and ky10.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev11 group by ontology, query) as ky11 on ky11.ontology = rap.ontology and ky11.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev12 group by ontology, query) as ky12 on ky12.ontology = rap.ontology and ky12.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev14 group by ontology, query) as ky14 on ky14.ontology = rap.ontology and ky14.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev15 group by ontology, query) as ky15 on ky15.ontology = rap.ontology and ky15.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ  from kyriev16 group by ontology, query) as ky16 on ky16.ontology = rap.ontology and ky16.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ , avg(timeOptUCQ) as timeOptUCQ, avg(sizeOptUCQ) as sizeOptUCQ from presto1 group by ontology, query) as pre1 on pre1.ontology = rap.ontology and pre1.query = rap.query
  left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ , avg(timeOptUCQ) as timeOptUCQ, avg(sizeOptUCQ) as sizeOptUCQ from presto2 group by ontology, query) as pre2 on pre2.ontology = rap.ontology and pre2.query = rap.query
  left outer join (select ontology, query, avg(time) as time, avg(sizequeries) as sizequeries, avg(sizejoins) as sizejoins, avg(sizeatoms) as sizeatoms from nyaya group by ontology, query) as nya on nya.ontology = rap.ontology and nya.query = rap.query
  left outer join (select ontology, query, avg(time) as time, avg(size) as size from clipper group by ontology, query) as cli on cli.ontology = rap.ontology and cli.query = rap.query
order by rap.ontology, rap.query''')
  tableContents = ''
  prev = ''
  for row in [tuple(map(pretty, e)) for e in c]:
    tableContents += rowTemplate%((' ' if row[0] == prev else 'firstrow', )+row)
    prev = row[0]
  ontemplate='\n      <div><input type="checkbox" name="ontology" value="%s" /><a href="./files/%s.owl">%s</a></div>'
  ontologies = ''
  c.execute('select distinct rap.ontology from rapid as rap order by rap.ontology')
  for row in [(e[0],e[0],e[0]) for e in c]:
    ontologies += ontemplate%row
  return mainTemplate%(ontologies, tableContents)

def neverending():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table*}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{c c c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}\\cline{3-20}
\\multicolumn{2}{c|}{}&\\multicolumn{4}{c|}{REQUIEM (F mode)}&\\multicolumn{4}{c|}{Presto}&\\multicolumn{4}{ c|}{Rapid (D\\&F modes)}&\\multicolumn{2}{c|}{Nyaya}&\\multicolumn{4}{c|}{kyrie} \\\\ \\cline{3-20}
\\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Only UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ} \\\\ \\hline
\\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&time&size&time&size&time&size&time&size&time&size&time&size&time&size&time&size&time&size \\\\ \\hline'''
  footer ='''\n\\end{tabular}
\\smallskip
\\caption{Extract of some evaluation results}
\\label{tab:shortresults}
\\vspace{-5pt}
\\end{table*}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query, rqr.timeData, rqr.sizeData, rqr.timeUCQ, rqr.sizeUCQ,
                                 pre.timeData, pre.sizeData, pre.timeUCQ, pre.sizeUCQ,
                                 rap.timeData, rap.sizeData, rap.timeUCQ, rap.sizeUCQ,
                                 nya.time, nya.sizequeries,
                                 kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ
               from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from rapid group by ontology, query) as rap
               join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from kyriev11 group by ontology, query) as kyr on kyr.ontology = rap.ontology and kyr.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from presto group by ontology, query) as pre on pre.ontology = rap.ontology and pre.query = rap.query
               left outer join (select ontology, query, avg(time) as time, avg(sizequeries) as sizequeries from nyaya group by ontology, query) as nya on nya.ontology = rap.ontology and nya.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from requiem group by ontology, query) as rqr on rqr.ontology = rap.ontology and rqr.query = rap.query
               where kyr.ontology = "A" OR kyr.ontology = "AX" OR kyr.ontology = "AXE"
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '3' else row[0], ) + row[1:] + ('\\hline' if row[1] == '5' else ' ',))
  res += footer
  return res
  
def datalogs():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
    %\\renewcommand{\\tabcolsep}{0.1cm}
    \\begin{table*}\\footnotesize
    %\\rowcolors{1}{white}{lightgray}
    \\centering
    \\begin{tabular}{c c c|c|c|c|c|c|c|c|c|c|c|}\\cline{3-12}
    \\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{REQUIEM(F)}&\\multicolumn{2}{c|}{Presto}&\\multicolumn{2}{ c|}{Rapid}&\\multicolumn{2}{c|}{Clipper}&\\multicolumn{2}{c|}{kyrie} \\\\ \\hline
    \\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&clauses&milliseconds&clauses&milliseconds&clauses&milliseconds&clauses&milliseconds&clauses&milliseconds \\\\ \\hline'''
  footer ='''\n\\end{tabular}
    \\smallskip
    \\caption{Results of the execution to obtain datalog}
    \\label{tab:datalogresults}
    \\vspace{-5pt}
    \\end{table*}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query, rqr.sizeData, rqr.timeData, pre.sizeData, pre.timeData, rap.sizeData, rap.timeData, cli.size, cli.time, kyr.sizeData, kyr.timeData
               from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from rapid group by ontology, query) as rap
               join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from kyriev15 group by ontology, query) as kyr on kyr.ontology = rap.ontology and kyr.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from presto2 group by ontology, query) as pre on pre.ontology = rap.ontology and pre.query = rap.query
               left outer join (select ontology, query, avg(time) as time, avg(size) as size from clipper group by ontology, query) as cli on cli.ontology = rap.ontology and cli.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from requiemF group by ontology, query) as rqr on rqr.ontology = rap.ontology and rqr.query = rap.query
               where kyr.ontology = "U" OR kyr.ontology = "UX"
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '5' else row[0], ) + row[1:] + ('\\hline' if row[1] == '9' else ' ',))
  res += footer
  return res
    
def ucqs():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
    %\\renewcommand{\\tabcolsep}{0.1cm}
    \\begin{table*}\\footnotesize
    %\\rowcolors{1}{white}{lightgray}
    \\centering
    \\begin{tabular}{c c c|c|c|c|c|c|c|c|c|c|c|}\\cline{3-12}
    \\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{REQUIEM(F)}&\\multicolumn{2}{c|}{Rapid}&\\multicolumn{2}{ c|}{Prexto}&\\multicolumn{2}{c|}{Nyaya}&\\multicolumn{2}{c|}{kyrie} \\\\ \\hline
    \\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&clauses&milliseconds&clauses&milliseconds&clauses&milliseconds&clauses&milliseconds&clauses&milliseconds \\\\ \\hline'''
  footer ='''\n\\end{tabular}
    \\smallskip
    \\caption{Results of the execution to obtain UCQ}
    \\label{tab:ucqsresults}
    \\vspace{-5pt}
    \\end{table*}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query, rqr.sizeUCQ, rqr.timeUCQ, rap.sizeUCQ, rap.timeUCQ, pre.sizeUCQ, pre.timeUCQ, nya.sizequeries, nya.time, kyr.sizeUCQ, kyr.timeUCQ
               from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from rapid group by ontology, query) as rap
               join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from kyriev15 group by ontology, query) as kyr on kyr.ontology = rap.ontology and kyr.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from presto2 group by ontology, query) as pre on pre.ontology = rap.ontology and pre.query = rap.query
               left outer join (select ontology, query, avg(time) as time, avg(sizequeries) as sizequeries from nyaya group by ontology, query) as nya on nya.ontology = rap.ontology and nya.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from requiemF group by ontology, query) as rqr on rqr.ontology = rap.ontology and rqr.query = rap.query
               where kyr.ontology = "U" OR kyr.ontology = "UX"
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '5' else row[0], ) + row[1:] + ('\\hline' if row[1] == '9' else ' ',))
  res += footer
  return res
  
def queries():
  mainTemplate = '''<html>
  <head>
    <title>Characteristics of the analysed queries</title>
    <link rel="stylesheet" type="text/css" href="files/style.css" />
    <script src="files/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="files/highlighter.js" type="text/javascript"></script>
  </head>
  <body>
    <a href="http://www.oeg-upm.net/"><img style="float:left" src="files/Logo_OEG.png" /></a>
    <h2>Evaluation results for query rewriting</h2>
    <p>This page contains the results obtained from the analysis of the queries usually considered for query rewriting evaluation. Choose the ontologies to compare and check the results. If you have questions or suggestions please write me at <img id="mail" src="files/youshoulddothistoo.png" /></p>
    <div id="menu"><div>Choose your options.</div><br/ >
      <div><b>Ontologies:</b></div>%s
    </div>
    <div id="tablecontainer">
      <table>
        <colgroup>
          <col />
          <col />
          <col class="startsSystem" />
          <col class="startsSystem" />
          <col />
          <col />
          <col class="startsSystem" />
          <col />
          <col />
          <col />
          <col />
          <col class="startsSystem" />
          <col class="startsSystem" />
          <col />
        </colgroup>
        <thead>
          <tr>
            <th>O</th>
            <th>q</th>
            <th>Length</th>
            <th>Distinguished variables</th>
            <th>Hanging variables</th>
            <th>Existential join variables</th>
            <th>Total number of paths</th>
            <th>Open paths</th>
            <th>Closed paths</th>
            <th>Joined paths</th>
            <th>Average path length</th>
            <th>Subquery divisibility</th>
            <th>q</th>
            <th>O</th>
          <tr>
        </thead>
        <tbody>%s
        </tbody>
      </table>
    </div>
    <div id="container"><div id="querysnippet"> </div></div>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
  </body>
</html>'''
  totalcols = 14
  rowTemplate = ''.join('<td>%s</td>' for i in range(totalcols))
  rowTemplate = '\n        <tr %s>%s</tr>'%('class="%s hidden" ontology="%s" query="%s"', rowTemplate)
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  c.execute('''select q.ontology, q.txt, q.ontology, q.number, q.length, q.distinguisedVars, q.hangingVars,
  q.existentialVars, q.paths, q.openPaths, q.closedPaths, q.joinedPaths, q.averageLengthPath, q.subqueries, 
  q.number, q.ontology from query as q order by q.ontology, q.number''')
  tableContents = ''
  prev = ''
  for row in [tuple(map(notsopretty, e)) for e in c]:
    tableContents += rowTemplate%((' ' if row[0] == prev else 'firstrow', )+row)
    prev = row[0]
  ontemplate='\n      <div><input type="checkbox" name="ontology" value="%s" /><a href="./files/%s.owl">%s</a></div>'
  ontologies = ''
  c.execute('select distinct ontology from query order by ontology')
  for row in [(e[0],e[0],e[0]) for e in c]:
    ontologies += ontemplate%row
  return mainTemplate%(ontologies, tableContents)

def neverendingpart1():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table*}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{c c c|c|c|c|c|c|c|c|c|c|c|c|}\\cline{3-14}
\\multicolumn{2}{c|}{}&\\multicolumn{4}{c|}{REQUIEM (F mode)}&\\multicolumn{4}{c|}{Presto}&\\multicolumn{4}{c|}{kyrie} \\\\ \\cline{3-14}
\\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ} \\\\ \\hline
\\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&time&size&time&size&time&size&time&size&time&size&time&size \\\\ \\hline'''
  footer ='''\n\\end{tabular}
\\smallskip
\\caption{First extract of some evaluation results}
\\label{tab:shortresults1}
\\vspace{-5pt}
\\end{table*}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query, rqr.timeData, rqr.sizeData, rqr.timeUCQ, rqr.sizeUCQ,
                                 pre.timeData, pre.sizeData, pre.timeUCQ, pre.sizeUCQ,
                                 kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ
               from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from rapid group by ontology, query) as rap
               join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from kyriev11 group by ontology, query) as kyr on kyr.ontology = rap.ontology and kyr.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from presto group by ontology, query) as pre on pre.ontology = rap.ontology and pre.query = rap.query
               left outer join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from requiemF group by ontology, query) as rqr on rqr.ontology = rap.ontology and rqr.query = rap.query
               where kyr.ontology = "A" OR kyr.ontology = "AX" OR kyr.ontology = "AXE"
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '3' else row[0], ) + row[1:] + ('\\hline' if row[1] == '5' else ' ',))
  res += footer
  return res

def neverendingpart2():
  header = '''{%\\renewcommand{\\arraystretch}{1.5}
\\renewcommand{\\tabcolsep}{0.1cm}
\\begin{table*}\\footnotesize
%\\rowcolors{1}{white}{lightgray}
\\centering
\\begin{tabular}{c c c|c|c|c|c|c|c|c|c|c|}\\cline{3-12}
\\multicolumn{2}{c|}{}&\\multicolumn{4}{ c|}{Rapid (D\\&F modes)}&\\multicolumn{2}{c|}{Nyaya}&\\multicolumn{4}{c|}{kyrie} \\\\ \\cline{3-12}
\\multicolumn{2}{c|}{}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ}&\\multicolumn{2}{c|}{Only UCQ}&\\multicolumn{2}{c|}{Datalog}&\\multicolumn{2}{c|}{UCQ} \\\\ \\hline
\\multicolumn{1}{|c|}{$\\mathcal{O}$}&\\multicolumn{1}{c|}{$q$}&time&size&time&size&time&size&time&size&time&size \\\\ \\hline'''
  footer ='''\n\\end{tabular}
\\smallskip
\\caption{Second extract of some evaluation results}
\\label{tab:shortresults2}
\\vspace{-5pt}
\\end{table*}}'''
  rowtemplate = '\n\\multicolumn{1}{|c|}{%s}&\\multicolumn{1}{c|}{%s}&%s&%s&%s&%s&%s&%s&%s&%s&%s&%s \\\\ %s'
  conn = sqlite3.connect('../results/evaluation.sqlite3')
  c = conn.cursor()
  res = header
  c.execute('''select kyr.ontology, kyr.query,
                                 rap.timeData, rap.sizeData, rap.timeUCQ, rap.sizeUCQ,
                                 nya.time, nya.sizequeries,
                                 kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ
               from (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from rapid group by ontology, query) as rap
               join (select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ from kyriev11 group by ontology, query) as kyr on kyr.ontology = rap.ontology and kyr.query = rap.query
               left outer join (select ontology, query, avg(time) as time, avg(sizequeries) as sizequeries from nyaya group by ontology, query) as nya on nya.ontology = rap.ontology and nya.query = rap.query
               where kyr.ontology = "A" OR kyr.ontology = "AX" OR kyr.ontology = "AXE"
               group by kyr.ontology, kyr.query order by kyr.ontology, kyr.query''')
  for row in [tuple(map(pretty, e)) for e in c]:
    res += rowtemplate%((" " if row[1] != '3' else row[0], ) + row[1:] + ('\\hline' if row[1] == '5' else ' ',))
  res += footer
  return res
  
def neverneverending():
  return neverendingpart1() + neverendingpart2()
  
  
if __name__ == "__main__":
  toGenerate = ([('maintable.tex', maintable), ('prepro.tex', prepro),
                 ('newcases.tex', newcases), ('others.tex', others),
                 ('index.html', html), ('extract.tex', neverending),
                 ('queryanalysis.html', queries), ('datalogs.tex', datalogs),
                 ('ucqs.tex', ucqs)])
  for (f, c) in toGenerate:
    with open(f, 'w') as o:
      o.write(c())
  
  