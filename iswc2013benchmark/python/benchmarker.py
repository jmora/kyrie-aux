#!/usr/bin/python3

import os, sqlite3, re, datetime, time, subprocess, threading, signal
from rapid import Rapid
from presto import Presto
from nyaya import Nyaya
from kyrie import Kyrie
from requiem import Requiem
from clipper import Clipper
from data import *

def doit(ontologies, tools, qs, times = 5):
  time.sleep(5)
  with sqlite3.connect('../results/evaluation.sqlite3') as db:
    for jd in [f(db.cursor()) for f in tools]:
      for o in ontologies:
        print('testing ' + o)
        jd.test(o, range(len(queries[o])) if qs is None else qs, times)
        db.commit()
      time.sleep(10) #take it easy
      
                     
if __name__ == "__main__":
  ontologies = list(data.queries.keys())
  tools = [lambda x: Kyrie(x, 15), lambda x: Rapid(x), lambda x: Requiem(x),
           lambda x: Presto(x, 1), lambda x: Presto(x, 2),
           lambda x: Nyaya(x), lambda x: Clipper(x)]
  doit(ontologies, tools, qs)
