from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal

class Clipper(OutputParser):
  def __init__(self, db):
    self.db = db
    self.db.execute('''create table if not exists clipper (
                              ontology text, query numeric,
                              time numeric, size numeric)''')

  def extract(self, l):
    for e in ([r"rewritten queries \+ rules : (?P<size>\d+)",
               r"total time : (?P<time>\d+)ms"]):
      m = re.match(e, l)
      if not m is None:
        return m.groupdict()
    return {}
  
  def test(self, ontology, qs, times):
    for q in qs:
      self.queryWrite(re.sub('<',':-',re.sub('-', '', queries[ontology][q]+'.')))
      for _ in range(times):
        self.__iteration(ontology,q+1)
      time.sleep(5)
  
  def __iteration(self, ontology, number):
    values = {}
    os.system("java -jar ../systems/clipper.jar -v 3 rewrite -cq query.txt  -d datalog.log -tq ../ontologies/%s.owl 1> output.txt 2>&1"%(ontology))
    time.sleep(1)
    with open("output.txt", 'r') as f:
      for l in f:
        values.update(self.extract(l))
    self.db.execute('insert into clipper values (?,?,?,?)', (ontology, number, int(values['time']), int(values['size'])))
