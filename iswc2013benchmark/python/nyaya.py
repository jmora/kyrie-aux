from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal, shutil, datetime

class Nyaya():
  def __init__(self, db):
    self.db = db
    self.db.execute('''create table if not exists nyaya (
                              ontology text, query integer,
                              time integer, sizequeries integer,
                              sizejoins integer, sizeatoms integer)''')
  
  def __extract(self, l):
    for e in ([r".* of queries\]:(?P<queries>\d+)",
               r".* of joins\]:(?P<joins>\d+)",
               r".* of atoms\]:(?P<atoms>\d+)",
               r".* Time: (?P<time>[\d\.]+)\[ms\]"]):
      m = re.match(e, l)
      if not m is None:
        return m.groupdict()
    return {}
  
  def test(self, ontology, qs, times):
    os.chdir('../systems/')
    with open('./nyaya/input/test-cases.txt', 'w') as fo:
      fo.write(ontology+'\n')
    self.__datalogize(ontology, qs)
    for _ in range(times):
      time.sleep(1)
      # os.makedirs('./nyaya/output', exist_ok=True) exist_ok not yet available...
      try:
        os.makedirs('./nyaya/output')
      except:
        pass
      os.system("java -jar nyaya.jar")
      self.__toDatabase(ontology, qs)
      time.sleep(1)
      try:
        shutil.move('./nyaya/output', './nyaya/output%s'%datetime.datetime.now().strftime('%Y%m%d%H%M%S'))
      except:
        shutil.rmtree('./nyaya/output', ignore_errors=True)
    os.chdir('../scripts/')
  
  def __toDatabase(self, ontology, qs):
    for q in qs:
      values = {}
      try:
        with open('./nyaya/output/%s/LTGDREW_Q%d_rew.dtg'%(ontology,q+1), 'r') as f:
          for l in f:
            values.update(self.__extract(l))
        self.db.execute('insert into nyaya values (?,?,?,?,?,?)', (ontology, q+1, int(float(values['time']) + 0.5), int(values['queries']),
                     int(values['joins']), int(values['atoms'])))
      except:
        pass
    self.db.close()
  
  def __datalogize(self, o, qs): #hardcoded paths, sorry for that. Actually, sorry for everything :/
    var = ['A','B','C','D','E','F']
    print('java -jar ./OWL2DatalogTranslator.jar ../ontologies/%s.owl ./nyaya/input/%s.dtg ./nyaya/input/%s_proc.owl --owl-ql 1> ./nyaya/input/%s_log.txt 2>&1 '%(o,o,o,o))
    os.system('java -jar ./OWL2DatalogTranslator.jar ../ontologies/%s.owl ./nyaya/input/%s.dtg ./nyaya/input/%s_proc.owl --owl-ql 1> ./nyaya/input/%s_log.txt 2>&1 '%(o,o,o,o))
    rqs = []
    hs = []
    for i in qs:
      pq = 'Q%d%s.\n'%(i+1, re.sub('<_', ':-', re.sub('-', '_', queries[o][i]))[1:])
      rqs.append((re.sub('\?(\d)', lambda x: '?'+var[int(x.group(1))], pq)))
      hs.append('?-Q%d%s.\n'%(i+1, re.search('\(([^\)]*)\)', rqs[-1]).group(0)))
    with open('./nyaya/input/%s.dtg'%o, 'a') as f:
      for i in rqs:
        f.write(i)
      f.write('\n')
      for i in hs:
        f.write(i)