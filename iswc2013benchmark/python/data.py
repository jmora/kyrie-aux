base = 'file:///D:/jmora/codigo/eclipse/phdws/newtest/'
base = 'file:///D:/jmora/My%20Dropbox/bignewtest/'

V =   (['Q(?0) <- Location(?0)',
        'Q(?0,?1) <- Military-Person(?0), hasRole(?1,?0), related(?0,?2)',
        'Q(?0,?1) <- Time-Dependent-Relation(?0), hasRelationMember(?0,?1), Event(?1)',
        'Q(?0,?1) <- Object(?0), hasRole(?0,?1), Symbol(?1)',
        'Q(?0) <- Individual(?0), hasRole(?0,?1), Scientist(?1), hasRole(?0,?2), Discoverer(?2), hasRole(?0,?3), Inventor(?3)',
        'Q(?0,?1,?2) <- Person(?0), hasRole(?0,?1), Leader(?1), exists(?1,?2)',
        'Q(?0,?1,?2) <- Person(?0), hasRole(?0,?1), Leader(?1), exists(?1,?2), TemporalInterval(?2), related(?0,?3), Country(?3)'])
P1_P5_P5X = (['Q(?0) <- edge(?0,?1)',
              'Q(?0) <- edge(?0,?1), edge(?1,?2)',
              'Q(?0) <- edge(?0,?1), edge(?1,?2), edge(?2,?3)',
              'Q(?0) <- edge(?0,?1), edge(?1,?2), edge(?2,?3), edge(?3,?4)',
              'Q(?0) <- edge(?0,?1), edge(?1,?2), edge(?2,?3), edge(?3,?4), edge(?4,?5)'])
S = (['Q(?0) <- StockExchangeMember(?0)',
      'Q(?0,?1) <- Person(?0), hasStock(?0,?1), Stock(?1)',
      'Q(?0,?1,?2) <- FinantialInstrument(?0), belongsToCompany(?0,?1), Company(?1), hasStock(?1,?2), Stock(?2)',
      'Q(?0,?1,?2) <- Person(?0), hasStock(?0,?1), Stock(?1), isListedIn(?1,?2), StockExchangeList(?2)',
      'Q(?0,?1,?2,?3) <- FinantialInstrument(?0), belongsToCompany(?0,?1), Company(?1), hasStock(?1,?2), Stock(?2), isListedIn(?1,?3), StockExchangeList(?3)',
      'Q(?0,?1) <- StockExchangeMember(?0), tradesOnBehalfOf(?0,?1), Company(?1), hasAddress(?1,?2)',
      'Q(?0) <- isExecutedBy(?0,?1), isExecutedFor(?0,?2), StockExchangeMember(?2), hasAddress(?2,?3), inverseofhasAddress(?3,?1)',
      'Q(?0) <- Investor(?0), hasStock(?0,?1), Stock(?1), Company(?2), hasStock(?2,?1)',
      'Q(?0,?1,?2) <- Investor(?0),  StockExchangeList(?1), Company(?2), hasStock(?2,?3), hasStock(?0,?3), Stock(?3), isListedIn(?3,?1)'])
U_UX = (['Q(?0) <- worksFor(?0,?1), affiliatedOrganizationOf(?1,?2)',
         'Q(?0,?1) <- Person(?0), teacherOf(?0,?1), Course(?1)',
         'Q(?0,?1,?2) <- Student(?0), advisor(?0,?1), FacultyStaff(?1), takesCourse(?0,?2), teacherOf(?1,?2), Course(?2)',
         'Q(?0,?1) <- Person(?0), worksFor(?0,?1), Organization(?1)',
         'Q(?0) <- Person(?0), worksFor(?0,?1), University(?1), hasAlumnus(?1,?0)',
         'Q(?0,?1) <- FacultyStaff(?0), headOf(?0,?2), College(?2), worksFor(?1,?2)',
         'Q(?0,?1) <- FacultyStaff(?0), headOf(?0,?2), Organization(?2), worksFor(?1,?2)',
         'Q(?0,?1) <- Professor(?0), teacherOf(?0,?1), GraduateCourse(?1)',
         'Q(?0,?1) <- Student(?0), Professor(?1), worksFor(?1,?2), memberOf(?0,?2), Faculty(?2)'])
A_AX = (['Q(?0) <- Device(?0), assistsWith(?0,?1)',
         'Q(?0) <- Device(?0), assistsWith(?0,?1), UpperLimbMobility(?1)',
         'Q(?0) <- Device(?0), assistsWith(?0,?1), Hear(?1), affects(?2,?1), Autism(?2)',
         'Q(?0) <- Device(?0), assistsWith(?0,?1), PhysicalAbility(?1)',
         'Q(?0) <- Device(?0), assistsWith(?0,?1), PhysicalAbility(?1), affects(?2,?1), Quadriplegia(?2)',
         'Q(?0) <- Device(?0), assistsWith(?0,?1), ReadingDevice(?1)',
         'Q(?0) <- Device(?0), assistsWith(?0,?1), ReadingDevice(?1), assistsWith(?1,?2), SpeechAbility(?2)'])
         
galen = (['Q(?0) <- Horn(?1), hasState(?0,?1), CellMorphologyState(?0)',
          'Q(?0) <- ShortBone(?1), CellMorphologyState(?1), hasState(?0,?1)',
          'Q(?0) <- hasState(?0,?1), TemporalUnit(?1), Tissue(?0)',
          'Q(?0) <- contains(?0,?1), contains(?0,?2), Metal(?1), Protozoa(?0), Steroid(?2)'])

core =(['Q(?0) <- userAdministeredBy(?0,?1), hasShopClass(?0,?2)',
        'Q(?0) <- bookItemHasBook(?0,?1), User(?0), hasShopClass(?0,?2)',
        'Q(?0) <- ShopEvent(?0), User(?1), hasRole(?0,?1)',
        'Q(?0) <- ShopEvent(?0), User(?1), hasRole(?0,?1), hasRole(?0,?1), Sport(?2)'])
        
tests = (['Q(?x,?v) <- A(?x,?y), B(?x,?z), C(?v)',
          'Q(?x) <- A(?x,?y), B(?y,?z), C(?z,?u), D(?z,?v), E(?v,?w), F(?w,?u), G(?v), H(?u)',
          'Q(?x,?y) <- pr(?x,?z), ps(?z,?y), pet(?z), pt(?y,?w), pr(?w,?v), pe(?v), ps(?w,?x), pt(?x,?t), pr(?t,?a)'])

queries = {'V': V, 'S': S, 'U': U_UX, 'A': A_AX, 'P1':P1_P5_P5X, 'P5': P1_P5_P5X, 'P5X': P1_P5_P5X, 'P5XE': P1_P5_P5X, 'UX': U_UX, 'UXE': U_UX, 'AX': A_AX, 'AXE': A_AX, 'AXEb': A_AX, 'AXEc': A_AX, 'core':core, 'galen-lite':galen, 'galen':galen}
