package it.uniroma1.dis.mora.preebox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubDataPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EBoxMapped {

	final static private Logger logger = LoggerFactory.getLogger(EBoxMapped.class);
	private static OWLDataFactory dataFactory = OWLManager.createOWLOntologyManager().getOWLDataFactory();

	private static OWLOntology loadOntology (String filepath) throws OWLOntologyCreationException {
		return OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(filepath));
	}

	public static String prettyName (String filepath, String toAppend) {
		int i = filepath.lastIndexOf("/") + 1;
		String resname = filepath.substring(0, i) + toAppend + filepath.substring(i);
		return resname;
	}

	public static String obtain (String tboxfile, String eboxfile) throws OWLOntologyCreationException, FileNotFoundException {
		OWLOntology tboxon = EBoxMapped.loadOntology(tboxfile);
		OWLOntology eboxon = EBoxMapped.loadOntology(eboxfile);
		Set<OWLEntity> mappedPredicates = tboxon.getSignature();
		Set<OWLEntity> notReallyMapped = EBoxMapped.getNotMappedEntities(eboxon);
		// EBoxMapped.logger.debug("size before removal: " + mappedPredicates.size());
		mappedPredicates.removeAll(notReallyMapped);
		mappedPredicates.remove(EBoxMapped.dataFactory.getOWLThing());
		EBoxMapped.insensitiveRemove(mappedPredicates, Arrays.asList(EBoxMapped.dataFactory.getOWLBottomDataProperty(), EBoxMapped.dataFactory.getOWLBottomObjectProperty())); // strangely exceptional
		// EBoxMapped.logger.debug("size after removal: " + mappedPredicates.size());
		String outputFile = EBoxMapped.prettyName(eboxfile, "mapped");
		EBoxMapped.write(mappedPredicates, outputFile);
		return outputFile;
	}

	private static Set<OWLEntity> getNotMappedEntities (OWLOntology eboxon) {
		HashSet<OWLEntity> res = new HashSet<OWLEntity>();
		for (OWLAxiom ax : eboxon.getAxioms())
			if (ax.isOfType(AxiomType.SUBCLASS_OF))
				res.addAll(EBoxMapped.getPredicates((OWLSubClassOfAxiom) ax));
			else if (ax.isOfType(AxiomType.SUB_OBJECT_PROPERTY))
				res.addAll(EBoxMapped.getPredicates((OWLSubObjectPropertyOfAxiom) ax));
			else if (ax.isOfType(AxiomType.SUB_DATA_PROPERTY))
				res.addAll(EBoxMapped.getPredicates((OWLSubDataPropertyOfAxiom) ax));
		// else if (ax.isOfType(AxiomType.SUB_PROPERTY_CHAIN_OF))
		// res.addAll(EBoxMapped.getPredicates((OWLSubPropertyChainOfAxiom) ax));
		return res;
	}

	private static Collection<? extends OWLEntity> getPredicates (OWLSubClassOfAxiom ax) {
		if (ax.getSuperClass().isOWLNothing())
			return ax.getSubClass().getSignature();
		return EBoxMapped.emptySet;
	}

	private static Collection<? extends OWLEntity> getPredicates (OWLSubObjectPropertyOfAxiom ax) {
		if (ax.getSuperProperty().isOWLBottomObjectProperty())
			return ax.getSubProperty().getSignature();
		return EBoxMapped.emptySet;
	}

	private static Collection<? extends OWLEntity> getPredicates (OWLSubDataPropertyOfAxiom ax) {
		if (ax.getSuperProperty().isOWLBottomDataProperty())
			return ax.getSubProperty().getSignature();
		return EBoxMapped.emptySet;
	}

	private static final Set<OWLEntity> emptySet = new HashSet<OWLEntity>();

	private static void insensitiveRemove (Set<OWLEntity> mappedPredicates, List<? extends Object> theList) {
		ArrayList<OWLEntity> toRemove = new ArrayList<OWLEntity>();
		for (OWLEntity m : mappedPredicates)
			for (Object r : theList)
				if (m.toString().equalsIgnoreCase(r.toString()))
					toRemove.add(m);
		for (OWLEntity m : toRemove)
			mappedPredicates.remove(m);
	}

	private static void write (Set<OWLEntity> mappedPredicates, String outputfile) throws FileNotFoundException {
		try (PrintStream writer = new PrintStream(outputfile)) {
			for (OWLEntity predicate : mappedPredicates)
				writer.println(predicate.toString());
		}
	}

	protected static Set<OWLEntity> getNotMappedEntitiesSemantically (OWLOntology eboxon) {
		OWLReasoner reasoner = new Reasoner.ReasonerFactory().createReasoner(eboxon);
		reasoner.precomputeInferences();
		Set<OWLEntity> res = null;
		res = EBoxMapped.addAll(res, reasoner.getBottomClassNode());
		res = EBoxMapped.addAll(res, reasoner.getBottomObjectPropertyNode());
		res = EBoxMapped.addAll(res, reasoner.getBottomDataPropertyNode());
		// res.addAll(Arrays.asList(EBoxMapped.dataFactory.getOWLTopDataProperty(), EBoxMapped.dataFactory.getOWLTopObjectProperty(), EBoxMapped.dataFactory.getOWLThing()));
		// EBoxMapped.logger.debug("not mapped:\n" + Convenience.mkstring(res, "\n", "//\n", "\n//\n\n"));
		return res;
	}

	private static Set<OWLEntity> addAll (Set<OWLEntity> res, Node<? extends OWLObject> node) {
		for (OWLObject entity : node.getEntities())
			res = EBoxMapped.nullAdd(res, entity.getSignature());
		return res;
	}

	private static Set<OWLEntity> nullAdd (Set<OWLEntity> res, Set<OWLEntity> signature) {
		if (res == null)
			return signature;
		res.addAll(signature);
		return res;
	}

}
