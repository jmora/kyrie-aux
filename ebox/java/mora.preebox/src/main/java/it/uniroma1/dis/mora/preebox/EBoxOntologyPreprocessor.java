package it.uniroma1.dis.mora.preebox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLNamedObject;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;
import org.semanticweb.owlapi.model.OWLSubPropertyChainOfAxiom;
import org.semanticweb.owlapi.model.OWLSymmetricObjectPropertyAxiom;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.normalisiert.utils.graphs.AdjacencyList;
import de.normalisiert.utils.graphs.StrongConnectedComponents;

public class EBoxOntologyPreprocessor {

	private OWLReasoner reasoner;
	final Logger logger = LoggerFactory.getLogger(EBoxOntologyPreprocessor.class);

	public String useEBoxForSCCs (String tboxfile, String eboxfile) throws Exception {
		OWLOntologyManager om = OWLManager.createOWLOntologyManager();
		OWLOntology eboxon = om.loadOntologyFromOntologyDocument(new File(this.cleanebox(eboxfile)));
		this.reasoner = new Reasoner.ReasonerFactory().createReasoner(eboxon);
		OWLOntology tboxon = om.loadOntologyFromOntologyDocument(new File(tboxfile));
		ArrayList<OWLAxiom> axioms = new ArrayList<OWLAxiom>(tboxon.getAxioms());
		boolean sccsremain = true;
		ArrayList<AtomDisgregation> dggs = new ArrayList<AtomDisgregation>();
		ArrayList<OWLAxiom> nonprocessed = new ArrayList<OWLAxiom>();
		ArrayList<OWLAxiom> processed = new ArrayList<OWLAxiom>();

		for (OWLAxiom axiom : axioms) {
			AtomDisgregation dgg = this.getGenericDisgregation(axiom);
			if (dgg.isEmpty())
				nonprocessed.add(axiom);
			else {
				/*
				 * logger.trace(axiom.toString()); logger.trace(dgg.toString());
				 */
				dggs.add(dgg);
				processed.add(axiom);
			}
		}
		// logger.debug("nonprocessed size {}", nonprocessed.size());
		while (sccsremain) {
			sccsremain = false;
			boolean[][] adj = this.getMatrix(dggs);
			// logger.debug("This is our matrix:\n{}", matrixdisp(adj));
			this.logger.debug("About to search for SCCs in {} axioms", adj.length);
			StrongConnectedComponents SCC = new StrongConnectedComponents(AdjacencyList.getAdjacencyList(adj));
			@SuppressWarnings("unchecked")
			Vector<Vector<Integer>> SCCs = SCC.getSCCs();
			/*
			 * logger.debug("Found {} SCCs", SCCs.size()); for (int h = 0; h < adj.length; h++) { String line = ""; for (Boolean b : adj[h]) if (b) line += "1"; else line += "0"; line += " " + h + " "
			 * + dggs.get(h).toString(); System.out.println(line); }
			 */
			for (Vector<Integer> scc : SCCs)
				// (System.out.print(scc.toString());
				if (!this.connections(scc, adj)) {
					// logger.debug("Found no connections");
					// System.out.print(" no connections! ");
					HashSet<OWLAxiom> candidate = new HashSet<OWLAxiom>();
					for (Integer i : scc)
						candidate.add(processed.get(i));
					if (this.reasoner.isEntailed(candidate)) {
						// System.out.print(" and entailed! ");
						sccsremain = true;
						Collections.sort(scc);
						for (int j = scc.size() - 1; j >= 0; j--) {
							processed.remove((int) scc.get(j));
							dggs.remove((int) scc.get(j));
						}
						break;
					}
				}
		}
		this.logger.info("Entailed SCCs removed from the original ontology");
		nonprocessed.addAll(processed);
		OWLOntologyManager om2 = OWLManager.createOWLOntologyManager();
		OWLOntology newtbox = om2.createOntology(new HashSet<OWLAxiom>(nonprocessed));
		String resname = this.prettyName(tboxfile, "preprocessed");
		om2.saveOntology(newtbox, new FileOutputStream(new File(resname)));
		return resname;
	}

	private String prettyName (String file, String prefix) {
		int i = file.lastIndexOf("/") + 1;
		return file.substring(0, i) + prefix + file.substring(i);
	}

	private String cleanebox (String eboxfile2) throws OWLOntologyCreationException, OWLOntologyStorageException, FileNotFoundException {
		HashSet<OWLAxiom> cleanedAxioms = new HashSet<OWLAxiom>();
		OWLOntologyManager om = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = om.loadOntologyFromOntologyDocument(new File(eboxfile2));
		for (OWLAxiom ax : ontology.getAxioms()) {
			boolean containsbottom = false;
			for (OWLEntity e : ax.getSignature())
				if (e.isBottomEntity()) {
					containsbottom = true;
					break;
				}
			if (!containsbottom)
				cleanedAxioms.add(ax);
		}
		OWLOntology neweboxon = this.createOntologyFromAxiomSet(cleanedAxioms, ontology, om);
		return this.saveontologytofile(neweboxon, eboxfile2, om);
	}

	private String saveontologytofile (OWLOntology neweboxon, String eboxfile2, org.semanticweb.owlapi.model.OWLOntologyManager om) throws OWLOntologyStorageException, FileNotFoundException {
		String newname = this.prettyName(eboxfile2, "clean");
		FileOutputStream stream = new FileOutputStream(new File(newname));
		om.saveOntology(neweboxon, stream);
		try {
			stream.close();
		} catch (IOException e) {
			return newname;
		}
		return newname;
	}

	private OWLOntology createOntologyFromAxiomSet (Set<OWLAxiom> axioms, OWLOntology sourceOntology, org.semanticweb.owlapi.model.OWLOntologyManager manager) throws OWLOntologyCreationException {
		IRI iri = sourceOntology.getOntologyID().getOntologyIRI();
		if (iri == null)
			iri = sourceOntology.getOntologyID().getDefaultDocumentIRI();
		iri = IRI.create(iri.toString() + "clean");
		OWLDataFactory factory = manager.getOWLDataFactory();
		for (OWLClass owlClass : sourceOntology.getClassesInSignature())
			axioms.add(factory.getOWLDeclarationAxiom(owlClass));
		for (OWLObjectProperty objectProperty : sourceOntology.getObjectPropertiesInSignature())
			axioms.add(factory.getOWLDeclarationAxiom(objectProperty));
		for (OWLDataProperty dataProperty : sourceOntology.getDataPropertiesInSignature())
			axioms.add(factory.getOWLDeclarationAxiom(dataProperty));
		OWLOntology targetOntology = manager.createOntology(axioms, iri);
		manager.setOntologyFormat(targetOntology, manager.getOntologyFormat(sourceOntology));
		return targetOntology;
	}

	// so much for case classes
	private class AtomDisgregation {
		public ArrayList<String> head;
		public ArrayList<String> body;

		public AtomDisgregation() {
			this.head = new ArrayList<String>();
			this.body = new ArrayList<String>();
		}

		@Override
		public String toString () {
			return this.head.toString() + " <- " + this.body.toString();
		}

		public AtomDisgregation combine (AtomDisgregation that) {
			this.body.addAll(that.body);
			this.head.addAll(that.head);
			return this;
		}

		public boolean isEmpty () {
			return this.body.isEmpty() && this.head.isEmpty();
		}
	}

	public String sccdisp (Vector<Vector<Integer>> SCCs) {
		String res = "";
		for (Vector<Integer> scc : SCCs) {
			res += "[ ";
			for (Integer e : scc)
				res += e.toString() + " ";
			res += "], ";
		}
		return res;
	}

	private boolean[][] getMatrix (ArrayList<AtomDisgregation> dggs) {
		int l = dggs.size();
		boolean[][] res = new boolean[l][l];
		HashMap<String, ArrayList<Integer>> correspondences = new HashMap<String, ArrayList<Integer>>();
		for (int i = 0; i < dggs.size(); i++)
			for (String p : dggs.get(i).body) {
				if (!correspondences.containsKey(p))
					correspondences.put(p, new ArrayList<Integer>());
				correspondences.get(p).add(i);
			}
		for (int i = 0; i < dggs.size(); i++)
			for (String p : dggs.get(i).head)
				if (correspondences.containsKey(p))
					for (Integer j : correspondences.get(p))
						res[i][j] = true;
		return res;
	}

	protected String matrixdisp (boolean[][] m) {
		String res = "";
		for (boolean[] b1 : m) {
			for (boolean b2 : b1)
				if (b2)
					res += "1";
				else
					res += "0";
			res += "\n";
		}
		return res;
	}

	// so much for matchings
	private AtomDisgregation getGenericDisgregation (OWLAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		// if (owlAxiom.isOfType(AxiomType.ANNOTATION_ASSERTION)) return getDisgregation((OWLAnnotationAssertionAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.ANNOTATION_PROPERTY_DOMAIN)) return getDisgregation((OWLAnnotationPropertyDomainAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.ANNOTATION_PROPERTY_RANGE)) return getDisgregation((OWLAnnotationPropertyRangeAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.ASYMMETRIC_OBJECT_PROPERTY)) return getDisgregation((OWLAsymmetricObjectPropertyAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.CLASS_ASSERTION)) return getDisgregation((OWLClassAssertionAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DATA_PROPERTY_ASSERTION)) return getDisgregation((OWLDataPropertyAssertionAxiom) owlAxiom);
		if (owlAxiom.isOfType(AxiomType.SUB_OBJECT_PROPERTY))
			res.combine(this.getDisgregation((OWLSubObjectPropertyOfAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.DATA_PROPERTY_RANGE)) return getDisgregation((OWLDataPropertyRangeAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DATATYPE_DEFINITION)) return getDisgregation((OWLDatatypeDefinitionAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DECLARATION)) res.combine(getDisgregation((OWLDeclarationAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.DIFFERENT_INDIVIDUALS)) return getDisgregation((OWLDifferentIndividualsAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DISJOINT_CLASSES)) return getDisgregation((OWLDisjointClassesAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DISJOINT_DATA_PROPERTIES)) return getDisgregation((OWLDisjointDataPropertiesAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DISJOINT_OBJECT_PROPERTIES)) return getDisgregation((OWLDisjointObjectPropertiesAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.DISJOINT_UNION)) return getDisgregation((OWLDisjointUnionAxiom) owlAxiom);
		if (owlAxiom.isOfType(AxiomType.EQUIVALENT_CLASSES))
			res.combine(this.getDisgregation((OWLEquivalentClassesAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.EQUIVALENT_DATA_PROPERTIES)) return getDisgregation((OWLEquivalentDataPropertiesAxiom) owlAxiom);
		if (owlAxiom.isOfType(AxiomType.EQUIVALENT_OBJECT_PROPERTIES))
			res.combine(this.getDisgregation((OWLEquivalentObjectPropertiesAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.FUNCTIONAL_DATA_PROPERTY)) return getDisgregation((OWLFunctionalDataPropertyAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.FUNCTIONAL_OBJECT_PROPERTY)) return getDisgregation((OWLFunctionalObjectPropertyAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.HAS_KEY)) return getDisgregation((OWLHasKeyAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY)) return getDisgregation((OWLInverseFunctionalObjectPropertyAxiom) owlAxiom);
		if (owlAxiom.isOfType(AxiomType.INVERSE_OBJECT_PROPERTIES))
			res.combine(this.getDisgregation((OWLInverseObjectPropertiesAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.IRREFLEXIVE_OBJECT_PROPERTY)) return getDisgregation((OWLIrreflexiveObjectPropertyAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.NEGATIVE_DATA_PROPERTY_ASSERTION)) return getDisgregation((OWLNegativeDataPropertyAssertionAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.NEGATIVE_OBJECT_PROPERTY_ASSERTION)) return getDisgregation((OWLNegativeObjectPropertyAssertionAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.OBJECT_PROPERTY_ASSERTION)) return getDisgregation((OWLObjectPropertyAssertionAxiom) owlAxiom);
		if (owlAxiom.isOfType(AxiomType.OBJECT_PROPERTY_DOMAIN))
			res.combine(this.getDisgregation((OWLObjectPropertyDomainAxiom) owlAxiom));
		if (owlAxiom.isOfType(AxiomType.OBJECT_PROPERTY_RANGE))
			res.combine(this.getDisgregation((OWLObjectPropertyRangeAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.REFLEXIVE_OBJECT_PROPERTY)) return getDisgregation((OWLReflexiveObjectPropertyAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.SAME_INDIVIDUAL)) return getDisgregation((OWLSameIndividualAxiom) owlAxiom);
		// if (owlAxiom.isOfType(AxiomType.SUB_ANNOTATION_PROPERTY_OF)) return getDisgregation((OWLSubAnnotationPropertyOfAxiom) owlAxiom);
		if (owlAxiom.isOfType(AxiomType.SUB_PROPERTY_CHAIN_OF))
			res.combine(this.getDisgregation((OWLSubPropertyChainOfAxiom) owlAxiom));
		if (owlAxiom.isOfType(AxiomType.SUBCLASS_OF))
			res.combine(this.getDisgregation((OWLSubClassOfAxiom) owlAxiom));
		if (owlAxiom.isOfType(AxiomType.SYMMETRIC_OBJECT_PROPERTY))
			res.combine(this.getDisgregation((OWLSymmetricObjectPropertyAxiom) owlAxiom));
		if (owlAxiom.isOfType(AxiomType.DATA_PROPERTY_DOMAIN))
			res.combine(this.getDisgregation((OWLDataPropertyDomainAxiom) owlAxiom));
		// if (owlAxiom.isOfType(AxiomType.TRANSITIVE_OBJECT_PROPERTY)) return getDisgregation((OWLTransitiveObjectPropertyAxiom) owlAxiom);
		if (res.isEmpty())
			this.logger.warn("I don't know how to handle this type of axioms: {}", owlAxiom.getAxiomType().toString());
		return res;
	}

	protected AtomDisgregation getDisgregation (OWLDeclarationAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.add(this.getFName(owlAxiom.getEntity()));
		res.head.addAll(this.getName(owlAxiom.getSignature()));
		return res;
	}

	private Collection<String> getName (Set<OWLEntity> signature) {
		ArrayList<String> res = new ArrayList<String>();
		for (OWLEntity e : signature)
			res.add(this.getFName(e));
		return res;
	}

	private AtomDisgregation getDisgregation (OWLSubObjectPropertyOfAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getSubProperty()));
		res.head.addAll(this.getName(owlAxiom.getSuperProperty()));
		if (res.body.contains("AUX1"))
			this.logger.debug("here! {}", res.toString());
		return res;
	}

	private AtomDisgregation getDisgregation (OWLSubClassOfAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getSubClass()));
		res.head.addAll(this.getName(owlAxiom.getSuperClass()));
		return res;
	}

	/*
	 * private AtomDisgregation getDisgregation (OWLDataPropertyRangeAxiom owlAxiom) { AtomDisgregation res = new AtomDisgregation(); res.body.addAll(getName(owlAxiom.getProperty()));
	 * res.head.addAll(getName(owlAxiom.getRange())); return res; }
	 * 
	 * private ArrayList<String> getName (OWLDataRange range) { return null; }
	 */

	private AtomDisgregation getDisgregation (OWLObjectPropertyRangeAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getProperty()));
		res.head.addAll(this.getName(owlAxiom.getRange()));
		return res;
	}

	private AtomDisgregation getDisgregation (OWLObjectPropertyDomainAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getProperty()));
		res.head.addAll(this.getName(owlAxiom.getDomain()));
		return res;
	}

	private AtomDisgregation getDisgregation (OWLSymmetricObjectPropertyAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getProperty()));
		res.head.addAll(this.getName(owlAxiom.getProperty()));
		return res;
	}

	private AtomDisgregation getDisgregation (OWLSubPropertyChainOfAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getPropertyChain()));
		res.head.addAll(this.getName(owlAxiom.getSuperProperty()));
		return res;
	}

	private AtomDisgregation getDisgregation (OWLInverseObjectPropertiesAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.body.addAll(this.getName(owlAxiom.getSecondProperty()));
		res.body.addAll(this.getName(owlAxiom.getFirstProperty()));
		res.head.addAll(res.body);
		return res;
	}

	private AtomDisgregation getDisgregation (OWLEquivalentObjectPropertiesAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		for (OWLObjectPropertyExpression o : owlAxiom.getProperties())
			res.body.addAll(this.getName(o));
		res.head.addAll(res.body);
		return res;
	}

	private AtomDisgregation getDisgregation (OWLEquivalentClassesAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		for (OWLClassExpression c : owlAxiom.getClassExpressions())
			res.body.addAll(this.getName(c));
		res.head.addAll(res.body);
		return res;
	}

	private AtomDisgregation getDisgregation (OWLDataPropertyDomainAxiom owlAxiom) {
		AtomDisgregation res = new AtomDisgregation();
		res.head.addAll(this.getName(owlAxiom.getDomain()));
		res.body.addAll(this.getName(owlAxiom.getProperty()));
		return res;
	}

	/*
	 * private AtomDisgregation getDisgregation (OWLAnnotationPropertyRangeAxiom owlAxiom) { AtomDisgregation res = new AtomDisgregation(); res.body.addAll(getName(owlAxiom.getProperty())); return
	 * res; }
	 * 
	 * private AtomDisgregation getDisgregation (OWLAnnotationPropertyDomainAxiom owlAxiom) { AtomDisgregation res = new AtomDisgregation(); return res; }
	 */

	private ArrayList<String> getName (List<OWLObjectPropertyExpression> propertyChain) {
		ArrayList<String> res = new ArrayList<String>();
		for (OWLObjectPropertyExpression op : propertyChain)
			res.addAll(this.getName(op));
		return res;
	}

	private ArrayList<String> getName (OWLObjectPropertyExpression property) {
		ArrayList<String> res = new ArrayList<String>();
		for (OWLNamedObject ono : property.getSignature())
			res.add(this.getFName(ono));
		return res;
		/*
		 * for (OWLNamedObject ono : property.getClassesInSignature()) res.add(getFName(ono)); for (OWLNamedObject ono : property.getObjectPropertiesInSignature()) res.add(getFName(ono)); for
		 * (OWLNamedObject ono : property.getDataPropertiesInSignature()) res.add(getFName(ono));
		 */
	}

	private ArrayList<String> getName (OWLClassExpression classExpression) {
		ArrayList<String> res = new ArrayList<String>();
		for (OWLNamedObject ono : classExpression.getSignature())
			res.add(this.getFName(ono));
		return res;
		/*
		 * ArrayList<String> res = new ArrayList<String>(); for (OWLNamedObject ono : classExpression.getClassesInSignature()) res.add(getFName(ono)); for (OWLNamedObject ono :
		 * classExpression.getObjectPropertiesInSignature()) res.add(getFName(ono)); for (OWLNamedObject ono : classExpression.getDataPropertiesInSignature()) res.add(getFName(ono)); return res;
		 */
	}

	private String getFName (OWLNamedObject namedObject) {
		return namedObject.getIRI().getFragment();
	}

	private ArrayList<String> getName (OWLDataPropertyExpression property) {
		ArrayList<String> res = new ArrayList<String>();
		for (OWLNamedObject ono : property.getSignature())
			res.add(this.getFName(ono));
		return res;
		/*
		 * ArrayList<String> res = new ArrayList<String>(); for (OWLNamedObject ono : property.getClassesInSignature()) res.add(getFName(ono)); for (OWLNamedObject ono :
		 * property.getObjectPropertiesInSignature()) res.add(getFName(ono)); for (OWLNamedObject ono : property.getDataPropertiesInSignature()) res.add(getFName(ono)); return res;
		 */
	}

	public boolean connections (Vector<Integer> scc, boolean[][] adj) {
		for (Integer j : scc)
			for (int i = 0; i < adj.length; i++)
				if (adj[i][j] && !scc.contains(i))
					return true;
		return false;
	}
}
