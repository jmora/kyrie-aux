package it.uniroma1.dis.mora.preebox;

import java.util.Iterator;

public class Convenience {

	// Don't!!
	private Convenience() {
	}

	public static <T> String mkstring (Iterable<T> parts, String sep, String start, String end) {
		StringBuffer res = new StringBuffer(start);
		Iterator<T> piterator = parts.iterator();
		if (piterator.hasNext())
			res.append(piterator.next().toString());
		while (piterator.hasNext())
			res.append(sep + piterator.next().toString());
		res.append(end);
		return res.toString();
	}
}
