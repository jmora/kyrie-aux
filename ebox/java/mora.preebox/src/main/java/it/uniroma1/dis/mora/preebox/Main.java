package it.uniroma1.dis.mora.preebox;

public class Main {

	public static void main (String[] args) throws Exception {
		if (args[0].equals("preprocessed"))
			new EBoxOntologyPreprocessor().useEBoxForSCCs(args[1], args[2]);
		if (args[0].equals("mapped"))
			EBoxMapped.obtain(args[1], args[2]);
	}
}
