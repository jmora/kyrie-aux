package it.uniroma1.dis.mora.eboxgen;

import it.uniroma1.dis.mora.preebox.EBoxOntologyPreprocessor;
import junit.framework.TestCase;

import org.junit.Test;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class EBoxTests {
	private static final String tbox = "tests/everything.ttl";
	private static final String ebox = "tests/eeverything.ttl";

	@Test
	public void checkSize () throws Exception {
		String file = new EBoxOntologyPreprocessor().useEBoxForSCCs(EBoxTests.tbox, EBoxTests.ebox);
		TestCase.assertTrue(EBoxTests.getAxiomCount(file) == EBoxTests.getAxiomCount(EBoxTests.tbox) - 1);
	}

	private static int getAxiomCount (String file) throws OWLOntologyCreationException {
		return TestConvenience.loadOntology(file).getAxiomCount();
	}

}
