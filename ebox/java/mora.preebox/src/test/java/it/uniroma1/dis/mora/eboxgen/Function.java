package it.uniroma1.dis.mora.eboxgen;

public interface Function<A, B> {
	public B apply (A a);
}
