package it.uniroma1.dis.mora.eboxgen;

import it.uniroma1.dis.mora.preebox.EBoxMapped;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import org.junit.Test;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class MappedTests {
	private static final String tbox = "tests/everything.ttl";
	private static final String ebox = "tests/eeverything.ttl";

	@Test
	public void checkSize () throws OWLOntologyCreationException, FileNotFoundException {
		String filename = EBoxMapped.obtain(MappedTests.tbox, MappedTests.ebox);
		ArrayList<String> mappedPredicates = this.obtainMappedPredicates(filename, false);
		OWLOntology ontology = TestConvenience.loadOntology(MappedTests.tbox);
		int fullsize = ontology.getClassesInSignature().size() + ontology.getObjectPropertiesInSignature().size() + ontology.getDataPropertiesInSignature().size();
		TestCase.assertTrue(mappedPredicates.size() == fullsize - 3);
	}

	@Test
	public void checkContents () throws OWLOntologyCreationException, FileNotFoundException {
		String filename = EBoxMapped.obtain(MappedTests.tbox, MappedTests.ebox);
		ArrayList<String> mappedPredicates = this.obtainMappedPredicates(filename, false);
		OWLOntology ontology = TestConvenience.loadOntology(MappedTests.tbox);
		HashSet<String> allpredicates = new HashSet<String>();
		allpredicates.addAll(this.predicates(ontology.getClassesInSignature()));
		allpredicates.addAll(this.predicates(ontology.getObjectPropertiesInSignature()));
		allpredicates.addAll(this.predicates(ontology.getDataPropertiesInSignature()));
		boolean first = allpredicates.containsAll(mappedPredicates);
		boolean second = true;
		List<String> notMapped = Arrays.asList("hasProfessor", "PotentialCourse");
		for (String s : notMapped)
			second &= !mappedPredicates.contains(s);
		mappedPredicates.addAll(notMapped);
		boolean third = mappedPredicates.containsAll(allpredicates);
		TestCase.assertTrue(first && second && third);
	}

	private Set<String> predicates (Set<? extends OWLEntity> things) {
		HashSet<String> res = new HashSet<String>();
		for (OWLEntity t : things)
			if (!t.isBottomEntity() && !t.isTopEntity())
				res.add(this.iriToLocal(t.toString()));
		return res;
	}

	private ArrayList<String> obtainMappedPredicates (String file, boolean fullURIs) {
		File thefile = new File(file);
		FileLineIterable lines = new FileLineIterable(thefile);
		ArrayList<String> mappedPredicates;
		if (fullURIs)
			mappedPredicates = MappedTests.iterator2ArrayAndApply(lines, new Function<String, String>() {
				@Override
				public String apply (String a) {
					return a.substring(1, a.length() - 1);
				}
			});
		else
			mappedPredicates = MappedTests.iterator2ArrayAndApply(lines, new Function<String, String>() {
				@Override
				public String apply (String a) {
					return MappedTests.this.iriToLocal(a);
				}
			});
		try {
			lines.close();
		} catch (IOException e) {
			// I tried
		}
		thefile.delete();
		return mappedPredicates;
	}

	private String iriToLocal (String a) {
		return a.substring(Math.max(a.lastIndexOf('/'), a.lastIndexOf('#')) + 1, a.length() - 1);
	}

	public static <E, S> ArrayList<S> iterator2ArrayAndApply (Iterator<E> source, Function<E, S> f) {
		ArrayList<S> res = new ArrayList<S>();
		while (source.hasNext())
			res.add(f.apply(source.next()));
		return res;
	}
}
