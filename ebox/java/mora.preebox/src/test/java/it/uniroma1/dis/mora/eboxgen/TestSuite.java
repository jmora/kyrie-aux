package it.uniroma1.dis.mora.eboxgen;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ MappedTests.class, EBoxTests.class })
public class TestSuite {

}
