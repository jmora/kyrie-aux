package it.uniroma1.dis.mora.eboxgen;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

public class FileLineIterable implements Iterable<String>, Iterator<String>, Closeable {

	private String line = null;
	private BufferedReader reader;

	public FileLineIterable(File file) {
		try {
			this.reader = new BufferedReader(new FileReader(file));
			this.line = this.reader.readLine();
		} catch (IOException e) {
			this.line = null;
		}
	}

	@Override
	public boolean hasNext () {
		return this.line != null;
	}

	@Override
	public String next () {
		String tline = this.line;
		try {
			this.line = this.reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			this.line = null;
		}
		return tline;
	}

	@Override
	public void remove () {
	}

	@Override
	public void close () throws IOException {
		this.reader.close();
	}

	@Override
	public Iterator<String> iterator () {
		return this;
	}

}
