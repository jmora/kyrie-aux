package it.uniroma1.dis.mora.eboxgen;

import java.io.File;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class TestConvenience {

	static OWLOntology loadOntology (String filepath) throws OWLOntologyCreationException {
		return OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(filepath));
	}

}
