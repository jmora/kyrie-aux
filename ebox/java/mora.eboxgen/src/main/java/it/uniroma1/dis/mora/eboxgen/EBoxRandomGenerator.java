package it.uniroma1.dis.mora.eboxgen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEquivalentClassesAxiom;
import org.semanticweb.owlapi.model.OWLEquivalentObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLInverseObjectPropertiesAxiom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;
import org.semanticweb.owlapi.model.OWLSubObjectPropertyOfAxiom;


public class EBoxRandomGenerator {

	public static OWLDataFactory odfi = OWLManager.getOWLDataFactory();
	private static Random random = new Random();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ArgParser pargs = new ArgParser();
		
		if (!pargs.parse(args))
			return;
		double cover = pargs.getCover();
		double reverse = pargs.getReverse();
		Set<OWLAxiom> allAxioms = pargs.getOntology().getTBoxAxioms(true);
		ArrayList<OWLAxiom> axioms = new ArrayList<OWLAxiom>();
		HashSet<OWLAxiom> EBoxAxioms = new HashSet<OWLAxiom>();
		for (OWLAxiom oa: allAxioms){
			if(oa.isOfType(AxiomType.SUBCLASS_OF,
					AxiomType.SUB_OBJECT_PROPERTY,
					AxiomType.OBJECT_PROPERTY_RANGE,
					AxiomType.OBJECT_PROPERTY_DOMAIN,
					// AxiomType.DATA_PROPERTY_DOMAIN,
					AxiomType.INVERSE_OBJECT_PROPERTIES,
					AxiomType.EQUIVALENT_OBJECT_PROPERTIES,
					AxiomType.EQUIVALENT_DATA_PROPERTIES,
					AxiomType.EQUIVALENT_CLASSES))
				axioms.add(oa);
			else 
				System.out.println(oa.getAxiomType());
		}
		Collections.shuffle(axioms); // for better randomness
		int limit = (int) (axioms.size() * pargs.getSize());
		EBoxAxioms.addAll(getPredicates(pargs.getOntology()));
		for (int i = 0; i < limit; i++){
			if (i <= limit * cover){
				OWLAxiom oax = axioms.get(i);
				if(i <= limit * cover * reverse)
					EBoxAxioms.add(reverse(oax));
				else
					EBoxAxioms.add(oax);
			}
			else
				EBoxAxioms.add(odfi.getOWLSubClassOfAxiom(
						getOWLClass(axioms.get(i%axioms.size()), axioms, i),
						getOWLClass(axioms.get((i+1)%axioms.size()), axioms, i)
						));
		}
		OWLOntologyManager oomi = OWLManager.createOWLOntologyManager();
		try {
			File f = new File(getFilename(args));
			System.out.println(EBoxAxioms.size());
			//f.createNewFile();
			oomi.saveOntology(oomi.createOntology(EBoxAxioms), new FileOutputStream(f));
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
			System.err.println("Error xkcd1068a: I'm so sorry, that's never happened before.");
		} catch (OWLOntologyStorageException e) {
			e.printStackTrace();
			System.err.println("Error xkcd1068b: I'm so sorry, that's never happened before..");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("Error xkcd1068c: I'm so sorry, that's never happened before...");
		}
	}

	private static Collection<OWLAxiom> getPredicates (OWLOntology ontology) {
		ArrayList<OWLAxiom> res = new ArrayList<OWLAxiom>();
		for (OWLClass a: ontology.getClassesInSignature(true))
			res.add(odfi.getOWLDeclarationAxiom(a));
		for (OWLObjectProperty a: ontology.getObjectPropertiesInSignature(true))
			res.add(odfi.getOWLDeclarationAxiom(a));
		for (OWLDataProperty a: ontology.getDataPropertiesInSignature(true))
			res.add(odfi.getOWLDeclarationAxiom(a));
		return res;
	}

	private static String getFilename(String[] args) {
		int i = args[3].lastIndexOf("/")+1;
		return args[3].substring(0,i) + "ebox" + args[0] + "-" + args[1] + "-" + args[2] + args[3].substring(i);
	}

	private static OWLClassExpression getOWLClass(OWLAxiom oa, ArrayList<OWLAxiom> axioms, int i) {
		int jump = 11;
		if (axioms.size() % 11 == 0){
			if (axioms.size() % 13 == 0)
				jump = 17;
			else
				jump = 13;
		}
		if (oa.isOfType(AxiomType.SUBCLASS_OF)){
			OWLSubClassOfAxiom oax = (OWLSubClassOfAxiom) oa;
			if (flipCoin())
				return oax.getSubClass();
			return oax.getSuperClass();
		}
		if (oa.isOfType(AxiomType.SUB_OBJECT_PROPERTY)){
			OWLSubObjectPropertyOfAxiom oax = (OWLSubObjectPropertyOfAxiom) oa;
			OWLObjectPropertyExpression prop = oax.getSuperProperty();
			if (flipCoin())
				prop = oax.getSubProperty();
			if (flipCoin())
				return odfi.getOWLObjectSomeValuesFrom(prop, odfi.getOWLThing());
			return odfi.getOWLObjectSomeValuesFrom(prop, getOWLClass(axioms.get((i+jump)%axioms.size()), axioms, i));
		}
		if (oa.isOfType(AxiomType.OBJECT_PROPERTY_RANGE)){
			OWLObjectPropertyRangeAxiom oax = (OWLObjectPropertyRangeAxiom) oa;
			if (flipCoin())
				return oax.getRange();
			if (flipCoin())
				return odfi.getOWLObjectSomeValuesFrom(oax.getProperty(), odfi.getOWLThing());
			return odfi.getOWLObjectSomeValuesFrom(oax.getProperty(), getOWLClass(axioms.get((i+jump)%axioms.size()), axioms, i));
		}
		if (oa.isOfType(AxiomType.OBJECT_PROPERTY_DOMAIN)){
			OWLObjectPropertyDomainAxiom oax = (OWLObjectPropertyDomainAxiom) oa;
			if (flipCoin())
				return oax.getDomain();
			if (flipCoin())
				return odfi.getOWLObjectSomeValuesFrom(oax.getProperty(), odfi.getOWLThing());
			return odfi.getOWLObjectSomeValuesFrom(oax.getProperty(), getOWLClass(axioms.get((i+jump)%axioms.size()), axioms, i));			
		}
		if (oa.isOfType(AxiomType.INVERSE_OBJECT_PROPERTIES, AxiomType.EQUIVALENT_OBJECT_PROPERTIES)){
			OWLObjectPropertyExpression prop;
			ArrayList<OWLObjectPropertyExpression> props;
			if (oa.isOfType(AxiomType.INVERSE_OBJECT_PROPERTIES)){
				OWLInverseObjectPropertiesAxiom oax = (OWLInverseObjectPropertiesAxiom) oa;
				props = new ArrayList<OWLObjectPropertyExpression>(oax.getProperties());
			}
			else if (oa.isOfType(AxiomType.EQUIVALENT_OBJECT_PROPERTIES)) { 
				OWLEquivalentObjectPropertiesAxiom oax = (OWLEquivalentObjectPropertiesAxiom) oa;
				props = new ArrayList<OWLObjectPropertyExpression>(oax.getProperties());
			}
			else {
				props = new ArrayList<OWLObjectPropertyExpression>();
			}
			prop = props.get(((int) Math.random() * props.size())%props.size()); 
			if (flipCoin())
				return odfi.getOWLObjectSomeValuesFrom(prop, odfi.getOWLThing());
			return odfi.getOWLObjectSomeValuesFrom(prop, getOWLClass(axioms.get((i+jump)%axioms.size()), axioms, i));
		}
		if (oa.isOfType(AxiomType.EQUIVALENT_DATA_PROPERTIES)){
			return getOWLClass(axioms.get((i+jump)%axioms.size()), axioms, i);
		}
		if (oa.isOfType(AxiomType.EQUIVALENT_CLASSES)){
			OWLEquivalentClassesAxiom oax = (OWLEquivalentClassesAxiom) oa;
			ArrayList<OWLClassExpression> classes = new ArrayList<OWLClassExpression>(oax.getClassExpressions());
			return classes.get(((int) Math.random() * classes.size())%classes.size());
		}
		
		return null;
	}

	private static OWLAxiom reverse(OWLAxiom oa) {
		if (oa.isOfType(AxiomType.EQUIVALENT_OBJECT_PROPERTIES,
				AxiomType.EQUIVALENT_DATA_PROPERTIES,
				AxiomType.EQUIVALENT_CLASSES,
				AxiomType.INVERSE_OBJECT_PROPERTIES))
				return oa;
		if (oa.isOfType(AxiomType.SUBCLASS_OF)){
			OWLSubClassOfAxiom oax = (OWLSubClassOfAxiom) oa;
			return odfi.getOWLSubClassOfAxiom(oax.getSuperClass(), oax.getSubClass());
		}
		if (oa.isOfType(AxiomType.SUB_OBJECT_PROPERTY)){
			OWLSubObjectPropertyOfAxiom oax = (OWLSubObjectPropertyOfAxiom) oa;
			return odfi.getOWLSubObjectPropertyOfAxiom(oax.getSuperProperty(), oax.getSubProperty());
		}
		if (oa.isOfType(AxiomType.OBJECT_PROPERTY_RANGE)){
			OWLObjectPropertyRangeAxiom oax = (OWLObjectPropertyRangeAxiom) oa;
			return odfi.getOWLSubClassOfAxiom(oax.getRange(),
					odfi.getOWLObjectSomeValuesFrom(
							odfi.getOWLObjectInverseOf(oax.getProperty()), odfi.getOWLThing()));
		}
		if (oa.isOfType(AxiomType.OBJECT_PROPERTY_DOMAIN)){
			OWLObjectPropertyDomainAxiom oax = (OWLObjectPropertyDomainAxiom) oa;
			return odfi.getOWLSubClassOfAxiom(oax.getDomain(),
					odfi.getOWLObjectSomeValuesFrom(
							odfi.getOWLObjectInverseOf(oax.getProperty()), odfi.getOWLThing()));
		}
		/*if (oax.isOfType(AxiomType.DATA_PROPERTY_DOMAIN)){
			OWLDataPropertyDomainAxiom oax = (OWLDataPropertyDomainAxiom) oax;
			return odfi.getOWLSubClassOfAxiom(oax.getDomain(),
					odfi.getOWLObjectSomeValuesFrom(
							odfi.getOWLDataInverseOf(oax.getProperty()), odfi.getOWLThing()));
		}*/
		return oa;
	}
	
	/* private static String join(String sep, String[] list){
		StringBuilder res = new StringBuilder();
		if (list.length > 0)
			res.append(list[0]);
		for (int i = 1; i < list.length; i++){
			res.append(sep);
			res.append(list[i]);
		}
		return res.toString();
	}*/ 
	
	private static boolean flipCoin() {
	    return random.nextBoolean();
	}

}
