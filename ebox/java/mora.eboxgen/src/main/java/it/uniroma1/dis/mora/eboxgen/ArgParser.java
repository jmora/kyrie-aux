package it.uniroma1.dis.mora.eboxgen;

import java.io.File;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class ArgParser {

	private double cover;
	private double reverse;
	private double size;
	private OWLOntology ontology;
	
	public boolean parse (String[] args){
		try {
			this.setOntology(args[3]);
			this.setCover(Double.parseDouble(args[0]));
			this.setReverse(Double.parseDouble(args[1]));
			this.setSize(Double.parseDouble(args[2]));
		} catch(Exception e){
			System.out.println("Usage: java -jar thisjar.jar cover reverse size file.owl\n" +
					"where:\n"+
					"\tcover is a float representing how much of the EBox should be obtained from the ontology\n"+
					"\treverse is a float representing how many axioms should be reversed\n" +
					"\tsize is a float representing how big the EBox should be wrt the ontology\n" +
					"\tontology is a path to a OWL file where the TBox should be\n" +
					"All floats are meant to be between 0 and 1, but size may be greater than 1. Please try again.\n");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private void setSize(double size) {
		this.size = size;
	}

	public double getSize() {
		return size;
	}

	private void setReverse(double reverse) {
		this.reverse = reverse;
	}

	public double getReverse() {
		return reverse;
	}

	private void setCover(double cover) {
		this.cover = cover;
	}

	public double getCover() {
		return cover;
	}

	private void setOntology(String ontologyPath) throws OWLOntologyCreationException {
		this.ontology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(new File(ontologyPath));
	}

	public OWLOntology getOntology() {
		return ontology;
	}
	
}
