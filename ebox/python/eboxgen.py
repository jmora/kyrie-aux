from data import *
import os


if __name__ == '__main__':
  ontologies = ['S', 'U', 'UX', 'UXE', 'A', 'AX', 'AXE', 'AXEb', 'P1', 'P5', 'P5X', 'P5XE', 'V', 'core', 'galen', 'galen-lite', 'AXEc']
  for o in ontologies:
    for s in range(0,11,2):
      for c in range(0,11 if s else 1,2):
        for r in range(0,11 if c else 1,2):
          os.system('java -jar ./eboxgen.jar %.1f %.1f %.1f ../ontologies/%s.owl'%(c/10.0,r/10.0,s/10.0,o))
          
