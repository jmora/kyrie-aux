#!/usr/bin/python3

import os, sqlite3, re, datetime, time, subprocess, threading, signal
from kyriee import Kyriee
from data import *

def doit(ontologies, tools, qs, times = 5):
  time.sleep(5)
  with sqlite3.connect('evaluation.sqlite3') as db:
    for jd in [f(db.cursor()) for f in tools]:
      for o in ontologies:
        print('testing ' + o)
        jd.test(o, range(len(queries[o])) if qs is None else qs, times)
        db.commit()
      time.sleep(10) #take it easy
      
                     
if __name__ == "__main__":
  ontologies = ['S', 'U', 'UX', 'UXE', 'A', 'AX', 'AXE', 'AXEb', 'P1', 'P5', 'P5X', 'P5XE', 'V']
  #ontologies = ['S']
  tools = [lambda x: Kyriee(x, 20)]
  qs = None
  doit(ontologies, tools, qs, 1)
