'''Table descriptions'''

import sqlite3, itertools

version = 38 # very important parameter

'''create table if not exists kyrieev%d (
  ontology text, query numeric, timeData numeric, sizeData numeric,
  timeUCQ numeric, sizeUCQ numeric, ecover numeric, erev numeric,
  esize numeric)'''%version
'''create table if not exists kyrieeprev%d (
  ontology text, timePre numeric, sizePre numeric, sizeOrig numeric,
  ecover numeric, erev numeric, esize numeric)'''%version

  
latexonto = 'V'
#annoying quotes...
ontology = 'ontology'
display = 'display'
tdisplay = 'tdisplay'
size = 'esize'
query = 'query'
children = 'children'
selectable = 'selectable'
cover = 'ecover'
rev = 'erev'
conn = sqlite3.connect('evaluation.sqlite3')
measures = 'measures'
db = conn.cursor()


yaxis = ontology
xaxis = size


odimensions = [ontology, query, size, cover, rev]
dimensions = {ontology: {
                display: 'Ontologies',
                tdisplay: 'O',
                query: ['select distinct ontology from query order by ontology', 'select distinct ontology from kyrieev%d order by ontology'%version],
                children: [query],
                selectable : True
              },
              query: {
                display: query,
                tdisplay: 'q',
                query: ['select distinct ontology, query from query order by ontology, query where ontology ="%s"'],
                children: [],
                selectable : False
              },
              size: {
                display: 'EBox size',
                tdisplay: 'size',
                query: ['select distinct esize from kyrieev%d order by esize'%version],
                children: [cover],
                selectable : True
              },
              cover: {
                display: 'EBox cover',
                tdisplay: 'cover',
                query: ['select distinct ecover from kyrieev%d order by ecover'%version],
                children: [rev],
                selectable : True
              },
              rev: {
                display: 'EBox reversed',
                tdisplay: 'rev',
                query: ['select distinct erev from kyrieev%d order by erev'%version],
                children: [measures],
                selectable : True
              },
              measures : {
                display : 'measure',
                tdisplay: 'val',
                query: [
                  'create table if not exists simpledims (ordinalcode numeric primary key, name text)',
                  '''insert or replace into simpledims values (1, "preprocess time (ms)"), (2, "preprocess clauses"),
                       (3, "Datalog time (ms)"), (4, "Datalog clauses"), (5, "UCQ time (ms)"), (6, "UCQ clauses")''',
                  'select name, ordinalcode from simpledims order by ordinalcode'
                ],
                children : [],
                selectable : False
              }
      }

allResults = '''
select kyp.ontology, query.txt, kyp.ontology, query.number, kyp.sizeOrig,
kyp.timePre, kyp.sizePre, kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ,
  from (
    select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ
      from kyrieev20 group by ontology, query) as kyr
  join (
    select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieeprev20 group by ontology
  ) as kyp
    on
      kyp.ontology = kyr.ontology and kyp.ecover = kyr.ecover and kyp.esize = kyr.esize and kyp.erev = kyr.erev
  join query on kyr.ontology = query.ontology and kyr.query = query.number order by kyr.ontology, query.number, kyp.esize, kyp.ecover, kyp.erev
  '''
  
extract = '''select kyp.ontology, query.txt, kyp.ontology, query.number, kyp.sizeOrig,
kyp.timePre, kyp.sizePre, kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ,
  from (
    select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ
      from kyrieev20 group by ontology, query where ontology = '%s') as kyr
  join (
    select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre from kyrieeprev20 group by ontology
  ) as kyp
    on
      kyp.ontology = kyr.ontology and kyp.ecover = kyr.ecover and kyp.esize = kyr.esize and kyp.erev = kyr.erev
  join query on kyr.ontology = query.ontology and kyr.query = query.number order by kyr.ontology, query.number, kyp.esize, kyp.ecover, kyp.erev
  '''%latexonto

nastr = lambda e: "N/A" if e is None else str(int(float(e))) if all(map(lambda x: x in "0123456789.", str(e))) else str(e)
rep = lambda thing, times: [thing for i in range(times)]
flatten = lambda e : e

def buildmenu():
  yield '<div id="menu"><div>Choose your options.</div><br/ >\n'
  for d in [d for d in odimensions if dimensions[d][selectable]]:
    yield '<div><b>%s</b></div>\n'%(dimensions[d][display])
    for e in dimensions[d][query]:
      db.execute(e)
    if d == ontology:
      for r in db:
        yield '<div><input type="checkbox" classsel="%s%s" /><a href="./files/%s.owl">%s</a></div>\n'%(d,r[0],r[0],r[0])
    else:
      for r in db:
        yield '<div><input type="checkbox" classsel="%s%d" />%s</div>\n'%(d,r[0]*10,r[0])
  yield '</div>'

def sideheaders():
  res = ''
  pending = yaxis
  while pending:
    d = pending.pop(0)
    pending += dimensions[d][children]
    res += '<td>%s</td>'%dimensions[d][tdisplay]
  return '<table><tr>%s</tr></table>'%res

def bodyheaders():
  res = ''
  pending = xaxis
  while pending:
    d = pending.pop(0)
    pending += dimensions[d][children]
    res += '<td>%s</td>'%dimensions[d][tdisplay]
  return '<table><tr></tr></table>'

def generateCSSs():
  for d in [d for d in odimensions if dimensions[d][selectable]]:
    for e in dimensions[d][query]:
      db.execute(e)
      for r in db:
        name = '%s%s'%(d, r[0]) if d == ontology else '%s%d'%(d, r[0]*10)
        with open('files/csss/%s.css'%name,'w') as cssfile:
          cssfile.write('''.%s{
  display:none;        
}
'''%name)
        

def webpage():
  #generateCSSs()
  yield '''<html>
  <head>
    <title>Results of the execution using EBoxes</title>
    <link rel="stylesheet" type="text/css" href="files/style.css" />
    <script src="files/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="files/highlighter.js" type="text/javascript"></script>'''
  for s in range(0, 11, 2):
    yield '<link rel="stylesheet" type="text/css" href="files/csss/esize%d.css" id="esize%d" />'%(s,s)
  for c in range(0, 11, 2):
    yield '<link rel="stylesheet" type="text/css" href="files/csss/ecover%d.css" id="ecover%d" />'%(c,c)
  for r in range(0, 11, 2):
    yield '<link rel="stylesheet" type="text/css" href="files/csss/erev%d.css" id="erev%d" />'%(r,r)
  db.execute(dimensions[ontology][query][0])
  for o in db:
    yield '<link rel="stylesheet" type="text/css" href="files/csss/ontology%s.css" id="ontology%s" />'%(o[0],o[0])
  yield '''</head>
  <body>
    <a href="http://www.oeg-upm.net/"><img style="float:left" src="files/Logo_OEG.png" /></a>
    <h2>Evaluation results for query rewriting</h2>
    <p>This page contains the results obtained from the analysis of the impact of an EBox on query rewriting, using a set of ontologies and queries for them.
    Choose the ontologies and parameters of the EBox to compare and check the results. Alternatively you may prefer to check a summary in PDF <a href="./files/comparison.pdf">[here]</a>. To cite these results please cite the corresponding paper [not yet].
    <p>Used ontologies are available by clicking on their name (in the menu on the left), the EBoxes used are available as a <a href= "./files/eboxes.zip"> zip file [here]</a>.
    If you have questions or suggestions please write me at <img id="mail" src="files/youshoulddoittoo2.png" /></p>
  '''
  for e in buildmenu():
    yield e
  for e in buildtable():
    yield e
  yield '''
    <div id="container"><div id="querysnippet"> </div></div>
  </body>
</html>'''

def buildtable():
  yield '<div id="tablecontainer">\n<table>\n<colgroup>'
  colt = '\n'.join(rep('<col %s />',6))
  tht = '\n'.join(rep('''<th %s>
  <div class="oddhead">%s</div>
  <div>%s</div>
  </th><th %s>
  <div class="evenhead">%s</div>
  <div>%s</div>
  </th>''',3))
  thtvals = '%s:Size:Preprocessing time (ms):%s:%.1f:Preprocessing Clauses:%s:Cover:Datalog time (ms):%s:%.1f:Datalog Clauses:%s:Reversed:UCQ time (ms):%s:%.1f:UCQ Clauses'
  trt = ''
  qt = '''select query.ontology, query.number, kyp.timePre, kyp.sizePre, kyr.timeData, kyr.sizeData, kyr.timeUCQ, kyr.sizeUCQ
  from query left outer join (
    select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ
      from kyrieev%d where esize = %s and ecover = %s and erev = %s group by ontology, query
  ) as kyr on kyr.ontology = query.ontology and kyr.query = query.number left outer join (
    select ontology, avg(timePre) as timePre, avg(sizePre) as sizePre
      from kyrieeprev%d where esize = %s and ecover = %s and erev = %s group by ontology
  ) as kyp on kyp.ontology = query.ontology order by query.ontology, query.number '''%tuple(
  [version] + rep('%.1f', 3) + [version] + rep('%.1f', 3))
  mq = conn.cursor().execute('''select query.ontology, query.txt, query.ontology, query.number, kyp.sizeOrig
  from query left outer join (
    select ontology, avg(sizeOrig) as sizeOrig
      from kyrieeprev%d group by ontology
  ) as kyp on query.ontology = kyp.ontology order by query.ontology, query.number'''%version)
  mq2 = conn.cursor().execute('''select kyp.sizeOrig, query.number, query.ontology
  from query left outer join (
    select ontology, avg(sizeOrig) as sizeOrig
      from kyrieeprev%d group by ontology
  ) as kyp on query.ontology = kyp.ontology order by query.ontology, query.number'''%version)
  #superloop
  qs = []
  for e in rep('<col />', 3):
    yield e
  for s in range(0, 11, 2):
    for c in range(0, 11 if s else 1, 2):
      for r in range(0, 11 if c else 1, 2):
        att0 = 'class = "startSystem size%d esize%d erev%d" esize = "%d" ecover="%d" erev="%d"'%(s, c, r, s, c, r)
        atts = 'class = "size%d esize%d erev%d" esize = "%d" ecover="%d" erev="%d"'%(s, c, r, s, c, r)
        yield colt%tuple([att0] + rep(atts,5))
  for e in rep('<col />', 3):
    yield e
  yield '</colgroup>\n<thead>\n<tr>\n<th>O</th>\n<th>q</th>\n<th>size orig</th>\n'
  for s in range(0, 11, 2):
    for c in range(0, 11 if s else 1, 2):
      for r in range(0, 11 if c else 1, 2):
        cl = 'class="esize%d ecover%d erev%d"'%(s,c,r)
        yield tht%tuple((thtvals%(cl,cl,s/10.0,cl,cl,c/10.0,cl,cl,r/10.0)).split(':'))
        trt += '\n'.join(rep('<td class="esize%d ecover%d erev%d">%s</td>'%(s,c,r,'%s'),6))
        qs.append(conn.cursor().execute(qt%(s/10.0,c/10.0,r/10.0,s/10.0,c/10.0,r/10.0)))
  yield '<th>size orig</th>\n<th>q</th>\n<th>O</th>\n</tr>\n</thead>\n<tbody>\n'
  trt = '<tr class="ontology%s" query="%s" ><td>%s</td><td>%s</td><td>%s</td>\n' + trt + '<td>%s</td><td>%s</td><td>%s</td></tr>\n'
  #n = 0
  while True:
    res = mq.fetchone()
    #n += 1
    if res is None:
      break
    #schlong = [q.fetchone() for q in qs]
    #print("%d elephants: %s"%(n, str(schlong)))
    #yield trt%tuple(map(nastr, itertools.chain(res, (e for partres in schlong for e in partres[2:]), mq2.fetchone())))
    yield trt%tuple(map(nastr, itertools.chain(res, (e for q in qs for e in (q.fetchone())[2:]), mq2.fetchone())))
  yield '</tbody>\n</table>\n</div>'
    

def querycomposer(onto, ops, version):
  yield 'select query.number,'
  fields = ['kyp{n}.timePre', 'kyp{n}.sizePre', 'kyr{n}.timeData', 'kyr{n}.sizeData', 'kyr{n}.timeUCQ', 'kyr{n}.sizeUCQ']
  yield ', '.join((f.format(n=i) for f in fields for i in range(len(ops))))
  yield '\nfrom (select * from query where ontology = "%s") as query'%onto 
  for i in range(len(ops)):
    p = (i,version) + ops[i][1:]
    yield '''left outer join (
      select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ
        from kyrieev{1} where esize = {2} and ecover = {3} and erev = {4} group by ontology, query
    ) as kyr{0} on kyr{0}.ontology = query.ontology and kyr{0}.query = query.number left outer join (
      select ontology, avg(sizeOrig) as sizeOrig, avg(timePre) as timePre, avg(sizePre) as sizePre
        from kyrieeprev{1} where esize = {2} and ecover = {3} and erev = {4} group by ontology
    ) as kyp{0} on kyp{0}.ontology = query.ontology'''.format(*p)
  yield 'order by query.ontology, query.number'

def captionquerycomposer(onto, ops, version):
  yield 'select query.number,'
  fields = ['kyr{n}.timeData', 'kyr{n}.sizeData', 'kyr{n}.timeUCQ', 'kyr{n}.sizeUCQ']
  yield ', '.join((f.format(n=i) for f in fields for i in range(len(ops))))
  yield '\nfrom (select * from query where ontology = "%s") as query'%onto 
  for i in range(len(ops)):
    p = (i,version) + ops[i][1:]
    yield '''left outer join (
      select ontology, query, avg(timeData) as timeData, avg(sizeData) as sizeData, avg(timeUCQ) as timeUCQ, avg(sizeUCQ) as sizeUCQ
        from kyrieev{1} where esize = {2} and ecover = {3} and erev = {4} group by ontology, query
    ) as kyr{0} on kyr{0}.ontology = query.ontology and kyr{0}.query = query.number '''.format(*p)
  yield 'order by query.ontology, query.number'
  
def paperextract(onto = latexonto):
  # opts = [('$\\alpha$', 0.0,0.0,0.0), ('$\\beta$', 0.2,0.8,0.0), ('$\\gamma$', 0.8,0.2,0.0), ('$\\delta$', 0.8,0.8,0.0)]
  opts = [('I', 0.0,0.0,0.0), ('II', 0.2,0.8,0.0), ('III', 0.8,0.2,0.0), ('IV', 0.8,0.8,0.0)]
  yield '''  {
  %\\renewcommand{\\arraystretch}{1.5}
  %\\renewcommand{\\tabcolsep}{0.1cm}
  \\setlength{\\tabcolsep}{1pt}
  \\begin{center}
  \\begin{table}[!htb]
  \\footnotesize
  %\\rowcolors{2}{black!5}{}
  \\begin{tabular}{|c|c c c c:c c c c|c c c c:c c c c|c c c c:c c c c|} \\hline
  %\\rowcolor{black!15}
  \\multirow{4}{*}{\\rotable{\\hspace{1pt}\\footnotesize{query}\\hspace{-1pt}}}& \\multicolumn{4}{ c:}{\\footnotesize{Preprocessed}} & \\multicolumn{4}{ c|}{\\footnotesize{Preprocessing}}
  & \\multicolumn{4}{ c:}{\\footnotesize{Datalog}} & \\multicolumn{4}{ c|}{\\footnotesize{Datalog}} & \\multicolumn{4}{ c:}{\\footnotesize{UCQ}} & \\multicolumn{4}{ c|}{\\footnotesize{UCQ}} \\\\
  %\\rowcolor{black!10}
  &'''
  yield '&'.join(rep('\\multicolumn{4}{c|}{\\footnotesize{time(ms)}}&\\multicolumn{4}{c|}{\\footnotesize{size}}', 3))
  yield '''\\\\
  %\\rowcolor{black!10}
  & '''
  yield '&'.join(rep('&'.join(("\\footnotesize{%s}"%o[0] for o in opts)),6))
  yield '\\\\ \\hline\n'
  rowt = '&'.join(rep('%s',25)) + '\\\\\n'
  res = db.execute(' '.join(querycomposer(onto, opts, version)))
  for l in res:
    yield rowt%tuple(map(nastr, l))
  res = db.execute('select avg(sizeOrig) as sizeOrig from kyrieeprev%d where ontology = "%s"'%(version,onto))
  for l in res: # it's 4am and I don't have any Internet connection, I guess it's .next() but idkidc...
    clauses = "N/A" if l[0] is None else str(int(l[0]))
  yield '''\\hline
  \\end{tabular}
  \\caption{Results obtained for ontology %s (original size %s clauses)\n'''%(onto, clauses)
  yield '''with EBoxes %s with size %.1f and cover %.1f, %s with size %.1f and cover %.1f,
  %s with size %.1f and cover %.1f and %s with size %.1f and cover %.1f.
  Reverse is 0 for all cases. }
  \\label{tab:resultsevaebox}
  \\end{table}
  \\end{center}
  }'''%(tuple((e for t in opts for e in t[:3])))

def specialzip(opts, onto, version):
  for t in opts:
    for e in t[:3]:
      yield e
    q = 'select avg(sizePre), avg(timePre) from kyrieeprev{0} where ontology="{1}" and esize={2} and ecover={3} and erev={4}'
    for l in db.execute(q.format(*((version, onto) + t[1:]))):
      for e in l:
        yield "N/A" if e is None else str(int(e))
        
def captionpaperextract(onto = latexonto):
  # opts = [('$\\alpha$', 0.0,0.0,0.0), ('$\\beta$', 0.2,0.8,0.0), ('$\\gamma$', 0.8,0.2,0.0), ('$\\delta$', 0.8,0.8,0.0)]
  opts = [('I', 0.0,0.0,0.0), ('II', 0.2,0.8,0.0), ('III', 0.8,0.2,0.0), ('IV', 0.8,0.8,0.0)]
  yield '''  {
  %\\renewcommand{\\arraystretch}{1.5}
  %\\renewcommand{\\tabcolsep}{0.1cm}
  \\setlength{\\tabcolsep}{1pt}
  \\begin{center}
  \\begin{table}[!htb]
  \\footnotesize
  %\\rowcolors{2}{black!5}{}
  \\begin{tabular}{|c|c c c c:c c c c|c c c c:c c c c|} \\hline
  %\\rowcolor{black!15}
  \\multirow{4}{*}{\\rotable{\\hspace{1pt}\\footnotesize{query}\\hspace{-1pt}}}
  & \\multicolumn{4}{ c:}{\\footnotesize{Datalog}} & \\multicolumn{4}{ c|}{\\footnotesize{Datalog}} & \\multicolumn{4}{ c:}{\\footnotesize{UCQ}} & \\multicolumn{4}{ c|}{\\footnotesize{UCQ}} \\\\
  %\\rowcolor{black!10}
  &'''
  yield '&'.join(rep('\\multicolumn{4}{c|}{\\footnotesize{time(ms)}}&\\multicolumn{4}{c|}{\\footnotesize{size}}', 2))
  yield '''\\\\
  %\\rowcolor{black!10}
  & '''
  yield '&'.join(rep('&'.join(("\\footnotesize{%s}"%o[0] for o in opts)),4))
  yield '\\\\ \\hline\n'
  rowt = '&'.join(rep('%s',17)) + '\\\\\n'
  res = db.execute(' '.join(captionquerycomposer(onto, opts, version)))
  for l in res:
    yield rowt%tuple(map(nastr, l))
  res = db.execute('select avg(sizeOrig) as sizeOrig from kyrieeprev%d where ontology = "%s"'%(version,onto))
  for l in res: # it's 4am and I don't have any Internet connection, I guess it's .next() but idkidc...
    clauses = "N/A" if l[0] is None else str(int(l[0]))
  yield '''\\hline
  \\end{tabular}
  \\caption{Results obtained for ontology %s (original size %s clauses)\n'''%(onto, clauses)
  yield '''with EBoxes %s with size %.1f and cover %.1f (preprocessing time %s ms and preprocessed size %s), %s with size %.1f and cover %.1f (preprocessing time %s ms and preprocessed size %s),
  %s with size %.1f and cover %.1f (preprocessing time %s ms and preprocessed size %s) and %s with size %.1f and cover %.1f (preprocessing time %s ms and preprocessed size %s).
  Reverse is 0 for all cases. }
  \\label{tab:resultsevaebox}
  \\end{table}
  \\end{center}
  }'''%tuple(specialzip(opts, onto, version))

def getprevals(opts, onto, version):
    q = 'select avg(timePre), avg(sizePre) from kyrieeprev{0} where ontology="{1}" and esize={2} and ecover={3} and erev={4}'
    return tuple(map(lambda e: "N/A" if e is None else str(int(e)), db.execute(q.format(*((version, onto) + opts[1:]))).fetchone()))
  
def dualtablepaperextract(onto = latexonto):
  # opts = [('$\\alpha$', 0.0,0.0,0.0), ('$\\beta$', 0.2,0.8,0.0), ('$\\gamma$', 0.8,0.2,0.0), ('$\\delta$', 0.8,0.8,0.0)]
  opts = [('I', 0.0,0.0,0.0), ('II', 0.2,0.8,0.0), ('III', 0.8,0.2,0.0), ('IV', 0.8,0.8,0.0)]
  yield '''  {
  %\\renewcommand{\\arraystretch}{1.5}
  %\\renewcommand{\\tabcolsep}{0.1cm}
  \\setlength{\\tabcolsep}{1pt}
  \\begin{center}
  \\begin{table}[!htb]
  \\scriptsize
  %\\rowcolors{2}{black!5}{}
  \\begin{tabular}{|c|c c c c:c c c c|c c c c:c c c c|} \\hline
  %\\rowcolor{black!15}
  \\multirow{4}{*}{\\rotable{\\hspace{1pt}\\footnotesize{query}\\hspace{-1pt}}}
  & \\multicolumn{4}{ c:}{\\footnotesize{Datalog}} & \\multicolumn{4}{ c|}{\\footnotesize{Datalog}} & \\multicolumn{4}{ c:}{\\footnotesize{UCQ}} & \\multicolumn{4}{ c|}{\\footnotesize{UCQ}} \\\\
  %\\rowcolor{black!10}
  &'''
  yield '&'.join(rep('\\multicolumn{4}{c|}{\\footnotesize{time(ms)}}&\\multicolumn{4}{c|}{\\footnotesize{size}}', 2))
  yield '''\\\\
  %\\rowcolor{black!10}
  & '''
  yield '&'.join(rep('&'.join(("\\footnotesize{%s}"%o[0] for o in opts)),4))
  yield '\\\\ \\hline\n'
  rowt = '&'.join(rep('%s',17)) + '\\\\\n'
  res = db.execute(' '.join(captionquerycomposer(onto, opts, version)))
  for l in res:
    yield rowt%tuple(map(nastr, l))
  res = db.execute('select avg(sizeOrig) as sizeOrig from kyrieeprev%d where ontology = "%s"'%(version,onto))
  for l in res: # it's 4am and I don't have any Internet connection, I guess it's .next() but idkidc...
    clauses = "N/A" if l[0] is None else str(int(l[0]))
  yield '''\\hline
  \\end{tabular} \\begin{tabular}{|l|%s|} \\hline
  \\multicolumn{%d}{|c|}{Additional information} \\\\ \\hline\n'''%(' '.join('c' for i in opts), len(opts)+1)
  yield 'EBox&' + ' & '.join((e[0] for e in opts)) + '\\\\ \\hline\n'
  yield 'PT &'+'&'.join((getprevals(o, onto, version)[0] for o in opts)) + '\\\\\n'
  yield 'PS &'+'&'.join((getprevals(o, onto, version)[1] for o in opts)) + '\\\\\n'
  yield 'size &'+'&'.join((str(o[1]) for o in opts)) + '\\\\\n'
  yield 'cover &'+'&'.join((str(o[2]) for o in opts)) + '\\\\\n'
  yield 'rev &'+'&'.join((str(o[3]) for o in opts)) + '\\\\'
  
  # # to delete
  # yield '''\\hline
  # \\end{tabular} \\begin{tabular}{|l|%s|} \\hline
  # \\multicolumn{%d}{|c|}{Additional information} \\\\ \\hline\n'''%('|'.join('c' for i in opts), len(opts)+1)
  # yield '\\textbackslash&' + ' & '.join((e[0] for e in opts)) + '\\\\ \\hline\n'
  # yield 'PT &'+'&'.join((getprevals(o, onto, version)[0] for o in opts)) + '\\\\\n'
  # yield 'PS &'+'&'.join((getprevals(o, onto, version)[1] for o in opts)) + '\\\\\n'
  # yield 'size &'+'&'.join((str(o[1]) for o in opts)) + '\\\\\n'
  # yield 'cover &'+'&'.join((str(o[2]) for o in opts)) + '\\\\\n'
  # yield 'rev &'+'&'.join((str(o[3]) for o in opts)) + '\\\\'
  # # end too delete
  
  yield '\\hline \n\\end{tabular}\n\\caption{Results obtained for ontology %s (original size %s clauses)\n'%(onto, clauses)
  yield '''with EBoxes %s, %s, %s and %s, with the characteristics detailed on the right
  for the ``size'', ``cover'' and ``reverse'' parameters of the EBox.
  There PT is the preprocessing time in milliseconds and PS is the number of clauses obtained after the preprocessing stage.}
  \\label{tab:resultsevaebox}
  \\end{table}
  \\end{center}
  }'''%tuple((o[0] for o in opts))
    
  
def comparator(g=paperextract):
  for o in [e[0] for e in db.execute(dimensions[ontology][query][1])]:
    for l in g(o):
      yield l

def compacttablepaperextract(onto = latexonto):
  res = db.execute('select avg(sizeOrig) as sizeOrig from kyrieeprev%d where ontology = "%s"'%(version,onto))
  for l in res:
    clauses = 'N/A' if l[0] is None else str(int(l[0]))
  # opts = [('$\\alpha$', 0.0,0.0,0.0), ('$\\beta$', 0.2,0.8,0.0), ('$\\gamma$', 0.8,0.2,0.0), ('$\\delta$', 0.8,0.8,0.0)]
  opts = [('I', 0.0,0.0,0.0), ('II', 0.2,0.8,0.0), ('III', 0.8,0.2,0.0), ('IV', 0.8,0.8,0.0)]
  veryspecialtable =(['PT &'+'&'.join((getprevals(o, onto, version)[0] for o in opts)) + '&',
                      'PS &'+'&'.join((getprevals(o, onto, version)[1] for o in opts)) + '&',
                      'size &'+'&'.join((str(o[1]) for o in opts)) + '&',
                      'cover &'+'&'.join((str(o[2]) for o in opts)) + '&',
                      'rev &'+'&'.join((str(o[3]) for o in opts)) + '&',
                      '\\cline{0-4}\\multicolumn{1}{l}{PT:} &\\multicolumn{4}{l|}{preprocessing}&',
                      '\\multicolumn{1}{l}{}&\\multicolumn{4}{l|}{time in ms}&',
                      '\\multicolumn{1}{l}{PS:}&\\multicolumn{4}{l|}{clauses after}&',
                      '\\multicolumn{1}{l}{}&\\multicolumn{4}{l|}{preprocessing}&' ])
  
  yield '''
  \\begin{table}
  \\setlength{\\tabcolsep}{1pt}
  \\begin{adjustbox}{width=\\columnwidth,center}
  \\begin{tabular}{|l c c c c|c|c c c c:c c c c|c c c c:c c c c|} \\hline
  \\multicolumn{5}{|c|}{Query independent}&\\multirow{4}{*}{\\rotable{\\hspace{1pt}{query}\\hspace{-1pt}}}
  & \\multicolumn{4}{ c:}{{Datalog}} & \\multicolumn{4}{ c|}{{Datalog}} & \\multicolumn{4}{ c:}{{UCQ}} & \\multicolumn{4}{ c|}{{UCQ}} \\\\
  '''
  yield '\\multicolumn{5}{|c|}{information}&&'
  yield '&'.join(rep('\\multicolumn{4}{c|}{{time(ms)}}&\\multicolumn{4}{c|}{{size}}', 2))
  yield '\\\\\nEBox&' + ' & '.join((e[0] for e in opts)) + '&&'
  yield '&'.join(rep('&'.join(('{%s}'%o[0] for o in opts)),4))
  yield '\\\\ \\hline\n'
  rowt = '&'.join(rep('%s',17)) + '\\\\\n'
  res = list(db.execute(' '.join(captionquerycomposer(onto, opts, version))))
  for i  in range(len(res)):
    yield veryspecialtable[i]
    yield rowt%tuple(map(nastr, res[i]))
  yield '\\cline{6-22}' if len(res) > 5 else '\\hline'
  yield '''
  \\end{tabular}
  \\end{adjustbox}
  \\vspace{5pt}
  \\caption{Results obtained for ontology %s (original size %s clauses)
  '''%(onto, clauses)
  yield '''with EBoxes %s, %s, %s and %s.}
  \\label{tab:resultsevaebox}
  \\end{table}'''%tuple((o[0] for o in opts))

captioncomparator = lambda: comparator(captionpaperextract)
dualcomparator = lambda: comparator(dualtablepaperextract)
compactcomparator = lambda: comparator(compacttablepaperextract)


'''create table if not exists kyrieev%d (
  ontology text, query numeric, timeData numeric, sizeData numeric,
  timeUCQ numeric, sizeUCQ numeric, ecover numeric, erev numeric,
  esize numeric)'''%version
'''create table if not exists kyrieeprev%d (
  ontology text, timePre numeric, sizePre numeric, sizeOrig numeric,
  ecover numeric, erev numeric, esize numeric)'''%version


def plotcase(type, value, cover, size):
  for row in db.execute('select e{size}, avg(time{type}) from kyrieev{version} where erev = 0 and e{cover} = {value} group by e{size} order by e{size}'.format(version=version, type=type, value=value, cover=cover,size=size)):
    yield row

def sizeplotcase(type, value, cover, size):
  for row in db.execute('select e{size}, avg(size{type}) from kyrieev{version} where erev = 0 and e{cover} = {value} group by e{size} order by e{size}'.format(version=version, type=type, value=value, cover=cover,size=size)):
    yield row
    
beautifymax = lambda x: 10 * Math.ceil(x/10)
marks = ['o', 'asterisk', 'triangle', 'square', 'diamond', 'pentagon', 'star']


def plotter():
  maxvalues = {}
  for type in ['UCQ', 'Data']:
    maxvalues[type] = {}
    for (cover, size) in [('cover', 'size'),('size', 'cover')]:
      for value in (e/10.0 for e in range(0,11,2)):
        maxvalues[type][cover] = 0
        yield '\\begin{filecontents}{%s_all%ss_%s%.02f.data}\n%s\tavg time'%(type, size, cover, value, size)
        for l in plotcase(type, value, cover, size):
          maxvalues[type][cover] = max(maxvalues[type][cover], l[1])
          yield '\t'.join(map(str,l))
        yield '\\end{filecontents}'

  for type in ['UCQ', 'Data']:
    for (cover, size) in [('cover', 'size'),('size', 'cover')]:
      yield '\\begin{tikzpicture}'
      # for cover in (e/10.0 for e in range(0,11,2)):
      currentmax = maxvalues[type][cover]
      yield '''\\draw (0,0) -- coordinate (x axis mid) (1,0);
               \\draw (0,0) -- coordinate (y axis mid) (0,%d);
               \\foreach \\x in {0,...,10}
               \\draw (0.1\\x,1pt) -- (0.1\\x,-3pt)
               node[anchor=north] {0.1\\x};
               \\foreach \\y in {0,%d,...,%d}
               \\draw (1pt,\\y) -- (-3pt,\\y) 
               node[anchor=east] {\\y};'''%(currentmax,currentmax/5,currentmax)
      for i in range(0,11,2):
        '\\draw plot[mark=%s] file {%s.data};'%(marks[i//2], '%s_all%ss_%s%.02f'%(type,size,cover,i/10.0))
      
      yield '\\begin{scope}[shift={(4,4)}]'
      for i in range(0,11,2):               
        yield '''\\draw[yshift=%d\\baselineskip] (0,0) -- 
                 plot[mark=%s] (0.25,0) -- (0.5,0)
                 node[right]{%s %f};'''%(i/2,marks[i//2],cover,i/10.0)
      yield '\\end{scope}\n\\end{tikzpicture}\n'

def betterplotter():
  for e in plotter():
    yield e+'\n'

    
    '''\\begin{semilogyaxis}['''


values = list((i/10.0 for i in range(0,11,2)))
fulltype = lambda type: 'Datalog' if type == 'Data' else 'UCQ'
countercase = ['\\hspace{5pt}','\n\n\\vspace{20pt}\n','\\hspace{5pt}','']

def oldpgfplotter():
  counter = 0
  for type in ['UCQ', 'Data']:
    for (cover, size) in [('cover', 'size'),('size', 'cover')]:
      yield  '''\\adjustbox{max width=\\plotsize}{
                  \\begin{minipage}[b]{\\minipagesize}
                  \\begin{tikzpicture}
                  \\begin{semilogyaxis}[
                    xlabel=%s,
                    ylabel=Average time (ms) to produce the %s rewriting,
                    legend columns=%d
                  ]
                  \\addlegendimage{empty legend}
                  '''%('%s of the EBox'%(size), fulltype(type), len(values)+2)
      for i in range(len(values)):
        yield '\n\n\\addplot plot coordinates {\n'
        for row in plotcase(type, values[i], cover, size):
          yield str(row)+'\n'
        yield '};'
      
      yield ('\n\\legend{%s,'%cover) + (',').join(map(lambda x: '%.1g'%x, values)) + '}\n\\end{semilogyaxis}\n\\end{tikzpicture}'
      yield ('\n\\label{fig:plotof%swrt%sfor%s}'%(cover,size,type))+('\n\\subcaption{%s rewriting times for %s wrt %s}'%(fulltype(type),cover,size))+'\n\\end{minipage}\n}%s'%countercase[counter]
      counter +=1

def pgfplotter(case, axis = 'axis'):
  counter = 0
  for type in ['UCQ', 'Data']:
    for (cover, size) in [('cover', 'size'),('size', 'cover')]:
      yield  '''\\adjustbox{max width=\\plotsize}{
                  \\begin{minipage}[b]{\\minipagesize}
                  \\begin{tikzpicture}
                  \\begin{%s}[
                    xlabel=%s of the EBox,
                    ylabel=%s,
                    legend columns=%d
                  ]
                  \\addlegendimage{empty legend}
                  '''%(axis, size, ('Average clauses in the %s rewriting' if case == 'size' else 'Average time (ms) to produce the %s rewriting')%fulltype(type), len(values)+2)
      for i in range(len(values)):
        yield '\n\n\\addplot plot coordinates {\n'
        f = plotcase if case == 'time' else sizeplotcase
        for row in f(type, values[i], cover, size):
          yield str(row)+'\n'
        yield '};'
      
      yield ('\n\\legend{%s,'%cover) + (',').join(map(lambda x: '%.1g'%x, values)) + ('}\n\\end{%s}\n\\end{tikzpicture}'%axis)
      yield ('\n\\label{fig:plotof%swrt%sfor%s%s}'%(cover,size,type,case))+(('\n\\subcaption{%s rewriting times for %s wrt %s}' if case == 'time' else '\n\\subcaption{number of clauses in the %s rewriting for %s wrt %s}')%(fulltype(type),cover,size))+'\n\\end{minipage}\n}%s'%countercase[counter]
      counter +=1
      


if __name__ == "__main__":
  toGenerate = [('index.html', webpage), ('comparison.tex', compactcomparator)]
  # toGenerate = [('extract.tex', compacttablepaperextract), ('comparison.tex', compactcomparator)]
  # toGenerate = [('extract.tex', compacttablepaperextract)]
  toGenerate = [('extract.tex', compacttablepaperextract), ('pgfplot.tex', lambda: pgfplotter('time', 'semilogyaxis')), ('sizepgfplot.tex', lambda: pgfplotter('size', 'semilogyaxis'))]
  for (f, c) in toGenerate:
    with open(f, 'w') as o:
      for l in c():
        o.write(l)
  
