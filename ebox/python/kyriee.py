from data import *
from outputparser import OutputParser
import os, sqlite3, re, datetime, time, subprocess, threading, signal

class Kyriee(OutputParser):
  def __init__(self, db, v, maps = False):
    self.db = db
    self.v = v
    self.maps = maps
    self.db.execute('''create table if not exists kyrieev%d (
                              ontology text, query numeric,
                              timeData numeric, sizeData numeric,
                              timeUCQ numeric, sizeUCQ numeric,
                              ecover numeric, erev numeric,
                              esize numeric)'''%v)
    self.db.execute('''create table if not exists kyrieeprev%d (
                              ontology text, timePre numeric,
                              sizePre numeric, sizeOrig numeric,
                              ecover numeric, erev numeric,
                              esize numeric)'''%v)


  def test(self, ontology, qs, times):
    for q in qs:
      self.queryWrite(queries[ontology][q])
      for _ in range(times):
        try:
          self.__iteration(ontology,q+1)
        except Exception as e:
          print("Error:" + str(e))
      time.sleep(5)
  
  def __iteration(self, ontology, number):
    for s in range(0,11,2):
      for c in range(0,11 if s > 1 else 1,2):
        for r in range(0,11 if c > 1 else 1,2):
          try:
            self.db.commit()
            print("%s cover %.1f rev %.1f size %.1f q%d"%(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), c/10.0,r/10.0,s/10.0,number))
            os.system("java -jar ./kyrieev%d.jar query.txt ../ontologies/%s.owl ../ontologies/ebox%.1f-%.1f-%.1f%s.owl > output.txt"%(self.v, ontology, c/10.0, r/10.0, s/10.0, ontology))
            values = {'ecover': c/10.0, 'erev': r/10.0, 'esize': s/10.0}
            with open("output.txt", 'r') as f:
              for l in f:
                values.update(self.extract(l))
            if (number == 1):
              self.db.execute('insert into kyrieeprev%d values (?,?,?,?,?,?,?)'%self.v, (ontology,
                               self.getMilis(values['timeLoading'], values['timePreprocessing']),
                               values['sizePreprocessing'], values['sizeLoading'], values['ecover'],
                               values['erev'], values['esize']))
            self.db.execute('insert into kyrieev%d values (?,?,?,?,?,?,?,?,?)'%self.v, (ontology, number,
                             self.getMilis(values['timeReloading'], values['timeSaturation']), values['sizeSaturation'],
                             self.getMilis(values['timeReloading'], values['timeUnfolding']), values['sizeUnfolding'], 
                             values['ecover'], values['erev'], values['esize']))
          except:
            continue
            
            
  def extract(self, l):
    r = {} # sorry for next line, but that's the way I like it.
    if (self.v <= 18):
      m = re.match(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+):(?P<second>\d+),(?P<microsecond>\d+).*PreprocessRewriter - (?P<stage>\w+) completed \((?P<size>\d+) clauses\)", l)
    else:
      m = re.match(r"(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+):(?P<second>\d+),(?P<microsecond>\d+).*Rewriter - (?P<stage>\w+) completed \((?P<size>\d+) clauses\)", l)
    if m is None:
      return r
    m = m.groupdict()
    #print(m)
    params = ['year', 'month', 'day', 'hour', 'minute', 'second', 'microsecond']
    d = {}
    for p in params:
      d[p] = int(m[p])
    d['microsecond'] = d['microsecond'] * 1000 # Yes, I lied.
    r['time'+m['stage']] = datetime.datetime(**d)
    r['size'+m['stage']] = int(m['size'])
    return r
