import sqlite3

def hand(query):
  l = query.split(';')
  # with sqlite3.connect('../results/evaluation.sqlite3') as conn:
  with sqlite3.connect('evaluation.sqlite3') as conn:
    c = conn.cursor()
    for e in l:
      i = 0
      print('Results for %s'%e)
      c.execute(e)
      for row in c:
        print(row)
        i += 1
      print('Obtained %d results for query\n%s\n'%(i, query))
    conn.commit()


if __name__ == '__main__':
  i = input()
  while i not in ['quit', 'exit', 'die', 'leave me alone']:
    try:
      hand(i)
    except:
      print("something was wrong on your query")
    i = input()
