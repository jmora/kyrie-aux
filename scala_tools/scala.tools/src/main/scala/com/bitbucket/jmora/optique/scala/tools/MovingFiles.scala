package com.bitbucket.jmora.optique.scala.tools

import java.io.File
import scala.io.Source
import scala.language.implicitConversions
import com.bitbucket.jmora.kyrie.scala.tools.Sink

object MovingFiles extends App {
  val original = "D:/programs/eclipsex/workspace/api.component.omm/src/main/xtend-gen/eu/optique/api/component/omm/analysis/global"
  val original2 = original.replace('/', '\\')
  val copy = "D:/code/optiquews/optique/src/eu/optique/api/component/omm/analysis/global"
  val previously = "lastTimeMovedFiles.txt"
  val forbidden = Set("import com.bitbucket.jmora.xtend.annotations.Extract;", "@Extract")

  implicit def stringToFile(s: String) = new File(s)

  def traverse(root: File): Seq[File] =
    if (root.isDirectory)
      root.listFiles.toSeq.map(traverse).foldLeft(Seq.empty[File])(_ ++ _)
    else
      Seq(root)

  def moved = traverse(original) filter { f => f.getName endsWith ".java" } map copy map { _.getPath }

  def copy(in: File): File = {
    val out = new File(in.getAbsolutePath.replace(original, copy).replace(original2, copy))
    Sink toFile out putLines (Source fromFile in getLines () filterNot { forbidden contains _ })
    out
  }

  Source fromFile previously getLines () foreach { s => new File(s).delete }
  Sink toFile previously putLines moved

}
