package com.bitbucket.jmora.kyrie.scala.tools

import com.hp.hpl.jena.graph.Node
import com.bitbucket.jmora.kyrie.scala.tools.datalog._
import com.hp.hpl.jena.query.Query
import com.hp.hpl.jena.sparql.algebra.Algebra
import com.hp.hpl.jena.sparql.algebra.Op
import com.hp.hpl.jena.sparql.algebra.op.OpProject
import com.hp.hpl.jena.sparql.algebra.op.OpBGP
import com.hp.hpl.jena.sparql.core.BasicPattern
import collection.JavaConversions._
import com.hp.hpl.jena.sparql.algebra.op.OpDistinct
import com.hp.hpl.jena.sparql.algebra.op.OpFilter
import com.hp.hpl.jena.sparql.algebra.op.OpOrder
import com.hp.hpl.jena.sparql.expr.ExprList
import com.hp.hpl.jena.sparql.expr.E_GreaterThan
import com.hp.hpl.jena.sparql.expr.ExprVar
import com.hp.hpl.jena.sparql.expr.nodevalue.NodeValueDT
import scala.collection.mutable.ArrayBuffer
import com.hp.hpl.jena.sparql.expr.E_LessThan
import com.hp.hpl.jena.sparql.expr.ExprFunction2
import com.hp.hpl.jena.graph.Node_Blank
import com.hp.hpl.jena.graph.Node_URI
import com.hp.hpl.jena.graph.Node_Variable
import com.hp.hpl.jena.graph.Node_ANY
import com.hp.hpl.jena.sparql.expr.NodeValue
import com.hp.hpl.jena.sparql.algebra.op.OpExtend
import com.hp.hpl.jena.sparql.expr.E_LogicalAnd
import com.hp.hpl.jena.sparql.algebra.op.OpJoin
import com.hp.hpl.jena.sparql.algebra.op.OpUnion
import com.hp.hpl.jena.sparql.algebra.op.OpLeftJoin
import com.hp.hpl.jena.sparql.algebra.op.OpGroup
import com.hp.hpl.jena.sparql.core.Var
import com.hp.hpl.jena.graph.Node_Literal
import com.bitbucket.jmora.kyrie.scala.tools.commons._
import com.hp.hpl.jena.sparql.expr.Expr

// there is a lot to repair here extending the expressiveness of Datalog, but that is beyond the current scope
case class ProtoClause(head: Map[String, Seq[String]], body: Seq[Atom])

class SPARQLtoDatalog(val fullURIs: Boolean) {

  val querySpecialPredicate = "Q"
  val counter = (1 to Int.MaxValue).iterator

  def convert(query: Query): Seq[Clause] = convertQuery(Algebra.compile(query)).map(starfix)
  private def q(): String = querySpecialPredicate // + counter.next
  private def newVar(): String = s"?namelessVariable${counter.next}"

  private def asVariable(v: Var): Variable = Variable(v.getVarName)

  private def cleanString(candidate: String): String =
    if (candidate startsWith "?")
      candidate.tail
    else
      candidate

  private def convertQuery(part: Op): Seq[ProtoClause] = part match {
    case p: OpProject => {
      val vars = p.getVars.map(_.getName).toSeq
      convertQuery(p.getSubOp).map(c => ProtoClause(
        vars.map(v => v -> c.head.getOrElse(v, Seq(v))).toMap,
        c.body)
      )
    }
    case p: OpUnion => convertQuery(p.getLeft) ++ convertQuery(p.getRight)
    case p: OpJoin => for { c <- convertQuery(p.getLeft); d <- convertQuery(p.getRight) }
      yield ProtoClause(c.head ++ d.head, c.body ++ d.body)
    case p: OpLeftJoin => {
      val left = convertQuery(p.getLeft)
      val right = for { c <- left; d <- convertQuery(p.getRight) }
        yield ProtoClause(c.head ++ d.head, c.body ++ d.body)
      left ++ right
    }
    case p: OpBGP      => protobodies(convertConditions(p.getPattern))
    case p: OpDistinct => convertQuery(p.getSubOp)
    case p: OpOrder    => convertQuery(p.getSubOp)
    case p: OpFilter => {
      val filterResult = convertConditions(p.getExprs)
      convertQuery(p.getSubOp).map(c => applyFilter(filterResult, c))
    }
    case p: OpExtend => { // Now I understand why lenses are good...
      val exprs: Map[String, Seq[String]] = p.getVarExprList.getExprs.map {
        case (k, v) => k.getName -> v.getVarsMentioned.map(getName(_)).toSeq
      }.toMap
      convertQuery(p.getSubOp).map(c => ProtoClause(c.head ++ exprs, c.body))
    }
    case p: OpGroup => convertQuery(p.getSubOp)
    case a          => notCovered(a, "convertQuery")
  }

  private def protobodies(atoms: Seq[Atom]): Seq[ProtoClause] = Seq(ProtoClause(Map(), atoms))

  private def notCovered(a: Any, m: String) = {
    println(s"$m: missing ${a.getClass.getCanonicalName}")
    Seq()
  }

  private def convertConditions(arg: Any): Seq[Atom] = arg match {
    case p: BasicPattern                 => p.getList.flatMap(convertConditions(_))
    case p: ExprList                     => p.getList.flatMap(convertConditions(_))
    case p: E_LogicalAnd                 => p.getArgs.flatMap(convertConditions(_))
    case p: ExprFunction2                => Seq(Atom(p.getFunctionSymbol.getSymbol, convertTerm(p.getArgs.flatMap(convertTerm(_)))))
    case p: com.hp.hpl.jena.graph.Triple => Seq(tripleToAtom(p))
    case a                               => notCovered(a, "convertConditions")
  }

  private def convertTerm(arg: Any): Seq[Term] = arg match {
    case p: Node_Literal   => Seq(Constant(getName(p)))
    case p: NodeValue      => Seq(Constant(getName(p.asNode())))
    case p: Var            => Seq(Variable(cleanString(p.getName)))
    case p: ExprVar        => Seq(Variable(getName(p.getAsNode)))
    case p: ArrayBuffer[_] => p.flatMap(convertTerm(_))
    case p: Term           => Seq(p)
    case a                 => notCovered(a, "convertTerm")
  }

  private def tripleToAtom(p: com.hp.hpl.jena.graph.Triple): Atom =
    if (p.getPredicate.getURI.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"))
      Atom(getName(p.getObject), convertTerm(p.getSubject))
    else
      Atom(getName(p.getPredicate), convertTerm(p.getSubject) ++ convertTerm(p.getObject))

  private def getName(node: Node): String = node match {
    case n: Node_Blank    => n.getBlankNodeLabel
    case n: Node_URI      => if (fullURIs) n.getURI else n.getLocalName
    case n: Node_Variable => n.getName
    case n: Node_Literal  => n.getLiteral.getLexicalForm
    case a                => notCovered(a, "getName").mkString("")
  }

  private def starfix(c: ProtoClause): Clause = Clause(
    if (c.head.size > 0)
      Atom(q, c.head.values.flatten.toSet.toSeq.sorted.map(Variable(_)))
    else
      Atom(q, variablesOf(c.body)),
    c.body)

  private def variablesOf(atoms: Seq[Atom]): Seq[Term] = atoms.flatMap(a => a.terms.filter(_.isInstanceOf[Variable])).toSet.toSeq.sorted
  private def variablesOf(atom: Atom): Seq[Term] = variablesOf(Seq(atom))

  private def applyFilter(filterResult: Seq[Atom], c: ProtoClause): ProtoClause = {
    val variables = variablesOf(c.body)
    ProtoClause(c.head, c.body ++ filterResult filter (a => variablesOf(a) forall (variables contains _)))
  }

}
