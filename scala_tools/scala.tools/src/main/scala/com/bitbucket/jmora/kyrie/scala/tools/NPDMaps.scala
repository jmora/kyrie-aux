package com.bitbucket.jmora.kyrie.scala.tools

import java.net.URI
import java.net.URL
import scala.collection.JavaConversions.asScalaSet
import scala.compat.Platform
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source
import scala.util.matching.Regex
import org.semanticweb.owlapi.apibinding._
import org.semanticweb.owlapi.model._
import com.bitbucket.jmora.kyrie.scala.tools.commons._
import java.io.PrintWriter
import java.io.PrintWriter

object NPDMaps {
  val base = "https://raw.githubusercontent.com/ontop/npd-benchmark/master/"
  def getNPDMapped():PrintWriter = {
    val signfut = Future { extractSignature(base + "ontology/npd-v2-ql_a.owl") }
    val file = Future { Source.fromURL(new URL(base + "mappings/mysql/spatial/npd-v2-ql_a.pretty.ttl")).getLines.toSeq }
    val cfut = file map extractClasses
    val pfut = file map extractPredicates
    val (cseq, cset) = Present(cfut)
    val (pseq, pset) = Present(pfut)
    val (csign, opsign, dpsign) = Present(signfut)
    val e = Future {
      using(new PrintWriter("npdebox.ttl")) { file =>
        createNonMapped(file, Seq((cset, csign), (pset, opsign), (pset, dpsign)))
      }
    }
    //println(cset mkString "\n")
    //println(pset mkString "\n")
    println(s"The number of class mentions is ${cseq.size}\n\tthe number of distinct classes is ${cset.size}\n\tin the signature we have ${csign.size}")
    println(s"The number of predicate mentions is ${pseq.size}\n\tthe number of distinct predicates is ${pset.size}\n\tin the signature we have ${opsign.size} object properties + ${dpsign.size} data properties = ${opsign.size + dpsign.size}")
    e
  }

  private val extractThing: (Regex) => (Seq[String]) => (Seq[String], Set[String]) = (regex) => (lines) => {
    val res = (lines flatMap (l => (regex findAllMatchIn l) flatMap (_.subgroups.headOption)))
    (res, res.toSet)
  }

  private val extractPredicates = extractThing("rr:predicate[^<]*<([^>]+)>".r)
  private val extractClasses = extractThing("rr:class[^<]*<([^>]+)>".r)

  private def extractSignature(ontologyURL: String): (Set[String], Set[String], Set[String]) = {
    val ontology = OWLManager.createOWLOntologyManager loadOntology (IRI create (URI create ontologyURL))
    val classes = Future { ontology getClassesInSignature false }
    val objectprops = Future { ontology.getObjectPropertiesInSignature }
    val dataprops = Future { ontology.getDataPropertiesInSignature }
    val res = Seq(classes, objectprops, dataprops).par.map(e => Present(e).map(_.getIRI.toString).toSet).seq
    (res(0), res(1), res(2))
  }

  private def createNonMapped(file: PrintWriter, elms: Seq[(Set[String], Set[String])]): PrintWriter = {
    Future {
      file println header
      file.flush
    }
    elms
      .zip(templates)
      .par
      .map { case ((mapped, sign), t) => sign diff mapped map t }
      .flatten
      .seq
      .foreach(file println _)
    file.flush
    file
  }

  private val templates: Seq[String => String] = Seq(
    e => { s"<$e> rdf:type owl:Class ;\n\trdfs:subClassOf owl:Nothing .\n" },
    e => { s"<$e> rdf:type owl:ObjectProperty ;\n\trdfs:subPropertyOf owl:bottomObjectProperty .\n" },
    e => { s"<$e> rdf:type owl:DatatypeProperty ;\n\trdfs:subPropertyOf owl:bottomDataProperty .\n" }
  )

  private val header = """@prefix : <http://sws.ifi.uio.no/vocab/npd-v2#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix ex: <http://example.org/ex#> .
@prefix isc: <http://resource.geosciml.org/classifier/ics/ischart/> .
@prefix nlx: <http://sws.ifi.uio.no/data/norlex/> .
@prefix npd: <http://sws.ifi.uio.no/data/npd-v2/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix ptl: <http://sws.ifi.uio.no/vocab/npd-v2-ptl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sql: <http://sws.ifi.uio.no/vocab/sql#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix geos: <http://www.opengis.net/ont/geosparql#> .
@prefix nlxv: <http://sws.ifi.uio.no/vocab/norlex#> .
@prefix npdv: <http://sws.ifi.uio.no/vocab/npd-v2#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix void: <http://rdfs.org/ns/void#> .
@prefix basic: <http://def.seegrid.csiro.au/isotc211/iso19103/2005/basic#> .
@prefix terms: <http://purl.org/dc/terms/> .
@prefix diskos: <http://sws.ifi.uio.no/data/diskos/> .
@prefix gts-30: <http://resource.geosciml.org/ontology/timescale/gts-30#> .
@prefix trs-30: <http://resource.geosciml.org/ontology/trs-30#> .
@prefix diskosv: <http://sws.ifi.uio.no/vocab/diskos#> .
@prefix gtrs-30: <http://resource.geosciml.org/ontology/timescale/gtrs-30#> .
@prefix geometry: <http://def.seegrid.csiro.au/isotc211/iso19107/2003/geometry#> .
@prefix sampling: <http://def.seegrid.csiro.au/isotc211/iso19156/2011/sampling#> .
@prefix temporal: <http://def.seegrid.csiro.au/isotc211/iso19108/2006/temporal#> .
@base <http://sws.ifi.uio.no/vocab/npd-v2> .

<http://sws.ifi.uio.no/vocab/npd-v2> rdf:type owl:Ontology .
"""
}
