package com.bitbucket.jmora.kyrie.scala.tools

import java.io.Closeable
import java.io.PrintWriter
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.Duration.Inf
import scala.language.implicitConversions

object commons {

  def using[T <% Closeable, R](resource: T)(block: (T => R)): R = {
    try {
      block(resource)
    } finally {
      resource.close()
    }
  }

  def filewrite(path: String, contents: String): Unit = {
    println(s"writing file $path")
    using(new PrintWriter(path)) { pw =>
      pw.write(contents)
      pw.flush()
    }
  }

  def makeFix[T](when: T => Boolean, then: T => T): (T => T) = in => if (when(in)) then(in) else in

  object Present {
    def apply[T](future: Future[T]): T = Await.result(future, Inf)
  }

  implicit def future2Present[T](future: Future[T]): T = Await.result(future, Inf)

}
