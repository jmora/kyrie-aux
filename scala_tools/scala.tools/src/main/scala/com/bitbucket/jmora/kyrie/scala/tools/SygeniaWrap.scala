package com.bitbucket.jmora.kyrie.scala.tools

import org.oxford.comlab.querygenerator.QTBGenerator
import java.util.{ Set => JSet }
import java.io.PrintWriter
import scala.io.Source
import java.io.File
import com.bitbucket.jmora.kyrie.scala.tools.commons._
import org.semanticweb.owl.apibinding.OWLManager
import java.net.URI
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import org.oxford.comlab.querygenerator.ChaseGenerator
import org.oxford.comlab.querygenerator.SubClassNormaliser
import org.semanticweb.owl.model.OWLObjectPropertyExpression
import org.oxford.comlab.datastructures.LabeledGraph
import org.semanticweb.owl.model.OWLClass
import org.semanticweb.owl.model.OWLIndividual
import org.oxford.comlab.datastructures.LabeledGraph
import com.bitbucket.jmora.kyrie.scala.tools.commons._

object SygeniaWrap {
  def generateQueries() =
    generateQTB(newFileFromURL(NPDMaps.base + "ontology/npd-v2-ql_a.owl"))

  def lastFragment(url: String) =
    url
      .reverse
      .takeWhile(_ != '/')
      .reverse
      .takeWhile(c => c != '#' && c != '?')

  def newFileFromURL(url: String): String = {
    val name = lastFragment(url)
    using(new PrintWriter(name)) { pw =>
      Source.fromURL(url).getLines().foreach(pw.println(_))
    }
    new File(name).toURI.toString
  }

  val manager = OWLManager.createOWLOntologyManager
  val factory = manager.getOWLDataFactory

  // Here starts the "wrap"
  def generateQTB(filepath: String): Seq[(Int, Int)] = {
    val subClassNormaliser = new SubClassNormaliser(factory)
    // load normalisation
    manager
      .loadOntologyFromPhysicalURI(URI create filepath)
      .getLogicalAxioms
      .foreach(_ accept subClassNormaliser)
    val chaseGenerator = new ChaseGenerator(factory)
    val folder = filepath.replaceAll(".owl", "") + "_QTB/"
    (new File(URI.create(folder))).mkdir()
    val queries = obtainQueries(
      chaseGenerator.constructChase(
        subClassNormaliser.getSubClassAxioms,
        subClassNormaliser.getSubPropertyAxioms
      ),
      chaseGenerator.getRootIndividuals,
      folder).filter(x => numberofatoms(x) % 10 > 4)

    queries.zipWithIndex
      .par
      .foreach(q => save(q._1, q._2, folder))
    queries.toSeq.map(numberofatoms).groupBy(identity).toSeq.map { case (n, nn) => (n, nn.size) }
  }

  private val atombodyregex = "\\([^\\)]+\\)".r.toString

  def numberofatoms(query: String): Int =
    query.split("<-").last.replaceAll(atombodyregex, "").split(",").size

  def obtainQueries(chase: LabeledGraph[OWLIndividual, OWLClass, OWLObjectPropertyExpression], rootIndividuals: JSet[OWLIndividual], qtbFolder: String): Set[String] = {
    val conjunctiveQueries = new java.util.TreeSet[String]
    rootIndividuals.foreach(
      generateQueriesForCurrentIndividual("Q(?0)<-", _, chase, conjunctiveQueries, rootIndividuals)
    )
    conjunctiveQueries.toSet
  }

  def generateQueriesForCurrentIndividual(currentCQString: String, currentIndv: OWLIndividual, chase: LabeledGraph[OWLIndividual, OWLClass, OWLObjectPropertyExpression], conjunctiveQueries: JSet[String], rootIndividuals: JSet[OWLIndividual], vcount: Int = 0): Unit = {
    val nodeAtomicConcepts = chase getLabelsOfNode currentIndv
    if (nodeAtomicConcepts != null)
      for (nodeLabel <- nodeAtomicConcepts)
        if (nodeLabel != null &&
          (!nodeLabel.isOWLThing) &&
          (!currentIndv.equals(factory.getOWLIndividual(URI.create("http://a_" + nodeLabel.getURI.getFragment)))))
          conjunctiveQueries.add(currentCQString + nodeLabel.getURI.getFragment + "(?" + vcount + ")")

    for (roleEdge <- chase.getSuccessors(currentIndv)) {
      val owlObjProperty = roleEdge.getEdgeLabel()
      if (!roleEdge.getToElement.equals(factory.getOWLIndividual(URI.create("http://d_" + owlObjProperty)))) {
        val newCQString = currentCQString + (if (!owlObjProperty.isAnonymous)
          roleEdge.getEdgeLabel + "(?" + vcount + ",?" + (vcount + 1) + ")"
        else
          roleEdge.getEdgeLabel.getInverseProperty.getSimplified + "(?" + (vcount + 1) + ",?" + vcount + ")"
        )
        conjunctiveQueries add newCQString
        generateQueriesForCurrentIndividual(newCQString + ", ", roleEdge.getToElement, chase, conjunctiveQueries, rootIndividuals, vcount + 1);
      }
      if (rootIndividuals.contains(roleEdge.getToElement) &&
        (!currentIndv.equals(factory.getOWLIndividual(URI.create("http://e_" + owlObjProperty)))) &&
        (!currentIndv.equals(factory.getOWLIndividual(URI.create("http://c_" + owlObjProperty))))) {
        val dCQstring = "Q(?0,?1)<-";
        if (!owlObjProperty.isAnonymous)
          conjunctiveQueries.add(dCQstring + roleEdge.getEdgeLabel() + "(?0,?1)");
        else
          conjunctiveQueries.add(dCQstring + roleEdge.getEdgeLabel().getInverseProperty().getSimplified() + "(?1,?0)");
      }
    }
  }

  def save(query: String, id: Int, folder: String) =
    Sink.toFile(URI.create(f"$folder%sq$id%04d.txt")).putLines(Seq(query))

}
