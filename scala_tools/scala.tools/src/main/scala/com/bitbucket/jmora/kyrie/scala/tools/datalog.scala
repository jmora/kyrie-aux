package com.bitbucket.jmora.kyrie.scala.tools

object datalog {

  sealed abstract class Term extends Ordered[Term] {
    import scala.math.Ordered.orderingToOrdered
    def compare(that: Term): Int = this.toString compare that.toString
  }

  case class Variable(name: String) extends Term {
    override def toString(): String = s"?$name"
  }

  case class Constant(value: String) extends Term {
    override def toString(): String = '"' + value + '"'
  }

  case class Function(name: String, on: Seq[Term]) extends Term {
    override def toString(): String = s"$name(${on.mkString(", ")})"
  }

  case class Atom(predicate: String, terms: Seq[Term]) {
    override def toString(): String = s"$predicate(${terms.mkString(", ")})"
  }

  case class Clause(head: Atom, body: Seq[Atom]) {
    override def toString(): String = s"$head <- ${body.mkString(", ")}"
  }

}
