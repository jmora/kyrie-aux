package com.bitbucket.jmora.kyrie.scala.tools

import com.hp.hpl.jena.query.QueryFactory
import org.oxford.comlab.querygenerator.QTBGenerator
import scala.io.Source
import com.bitbucket.jmora.kyrie.scala.tools.commons._
import java.io.PrintWriter
import java.net.URL
import java.io.File

object MyREPL {

  val conversor = new SPARQLtoDatalog(false)

  def makeURL(n: String): String = s"https://raw.githubusercontent.com/ontop/npd-benchmark/master/queries/$n.q"

  def convert(queryNumber: String): String = conversor.convert(QueryFactory.read(makeURL(queryNumber))).mkString("\n")

  def convertAndAppend(queryNumber: String): String = {
    val query = QueryFactory.read(makeURL(queryNumber))
    query.toString + "\n\n" + conversor.convert(query).mkString("\n")
  }

  def processNPD(): Unit = (1 to 21).map(n => f"$n%02d").par.foreach(n => filewrite(s"$n.q", convert(n)))

  def main(args: Array[String]) = {
    println("Hello World!")
    // NPDMaps.getNPDMapped
    Sink.toFile("stats.txt").putLines(
      SygeniaWrap.generateQueries.map { case (e, n) => s"$e\t$n" }
    )
    println("Good bye world!")
  }

}
